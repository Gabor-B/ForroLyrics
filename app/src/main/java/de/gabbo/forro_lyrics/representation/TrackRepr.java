package de.gabbo.forro_lyrics.representation;

import java.util.Map;
import java.util.TreeMap;

/**
 * Storage class for Track and lyric data
 */
public class TrackRepr implements Comparable<TrackRepr> {

	private String trackName;
	private Map<LangEnum, String> lyrics;

	public TrackRepr(String trackName) {
		this.trackName = trackName;
		lyrics = new TreeMap<>();
	}

	public String getName() {
		return trackName;
	}

	public void addLyric(LangEnum language, String lyric) {
		lyrics.put(language, lyric);
	}

	@Override
	public String toString() {
		return trackName + lyrics.keySet();
	}

	public Map<LangEnum, String> getLyrics() {
		return lyrics;
	}

	@Override
	public int compareTo(TrackRepr other) {
		return trackName.compareTo(other.getName());
	}

}
