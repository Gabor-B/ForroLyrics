package de.gabbo.forro_lyrics.representation;

import java.util.Set;
import java.util.TreeSet;

/**
 * Storage class for Album data
 */
public class AlbumRepr implements Comparable<AlbumRepr> {

	private final String albumName;
	private Set<TrackRepr> tracks;

	public AlbumRepr(String albumName) {
		this.albumName = albumName;
		this.tracks = new TreeSet<>();
	}

	public TrackRepr addTrack(String trackName) {
		TrackRepr album = new TrackRepr(trackName);
		for (TrackRepr storedTrack : this.tracks) {
			if (storedTrack.getName().equals(trackName)) {
				album = storedTrack;
				break;
			}
		}
		this.tracks.add(album);
		return album;
	}

	public String getName() {
		return albumName;
	}

	@Override
	public String toString() {
		return albumName + tracks;
	}

	public String getAlbumName() {
		return albumName;
	}

	public Set<TrackRepr> getTracks() {
		return tracks;
	}

	@Override
	public int compareTo(AlbumRepr other) {
		return albumName.compareTo(other.getAlbumName());
	}

	public void removeTrack(TrackRepr trackRepr) {
		tracks.remove(trackRepr);
	}

}
