package de.gabbo.forro_lyrics.representation;

/**
 * Enum for lyric languages
 */
public enum LangEnum {

	PT("Portuguese", "lang_pt", 0), EN("English", "lang_en", 1), DE("German", "lang_de", 2),
	FI("Finnish", "lang_fi", 3), FR("French", "lang_fr", 4), UNKN("Unknown", "lang_unkn", 5);
	
	private String language;
	private String resourceId;
	private Integer id;

	LangEnum(String language, String resourceId, Integer id) {
		this.language = language;
		this.resourceId = resourceId;
		this.id = id;
	}

	public String getResourceId() {
		return resourceId;
	}

	public String getLanguage() {
		return language;
	}

	public Integer getId() {
		return id;
	}

	public static LangEnum fromId(Integer id) {
		for (LangEnum lang : LangEnum.values()) {
			if (lang.id.equals(id)) {
				return lang;
			}
		}
		return null;
	}

}
