package de.gabbo.forro_lyrics.representation;

import java.util.Set;
import java.util.TreeSet;

/**
 * Storage class for Artist data
 */
public class ArtistRepr implements Comparable<ArtistRepr> {

	private String artistName;
	private Set<AlbumRepr> albums;

	public ArtistRepr(String artistName) {
		this.artistName = artistName;
		this.albums = new TreeSet<>();
	}

	public AlbumRepr addAlbum(String albumName) {
		AlbumRepr album = new AlbumRepr(albumName);
		for (AlbumRepr storedAlbum : this.albums) {
			if (storedAlbum.getName().equals(albumName)) {
				album = storedAlbum;
				break;
			}
		}
		this.albums.add(album);
		return album;
	}

	public Set<AlbumRepr> getAlbums() {
		return albums;
	}

	public String getName() {
		return artistName;
	}

	@Override
	public String toString() {
		return artistName + ": " + albums.toString();
	}

	@Override
	public int compareTo(ArtistRepr other) {
		return artistName.compareTo(other.getName());
	}

}
