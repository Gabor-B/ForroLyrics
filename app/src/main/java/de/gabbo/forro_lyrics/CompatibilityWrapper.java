package de.gabbo.forro_lyrics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.sqlite.SQLiteCursor;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.Locale;

/**
 * Wrapper around deprecated api functions
 * Created by gabor on 24.08.16.
 */
@SuppressWarnings("deprecation")
public class CompatibilityWrapper {

    protected static final int FIELD_TYPE_BLOB = 4;
    protected static final int FIELD_TYPE_FLOAT = 2;
    protected static final int FIELD_TYPE_INTEGER = 1;
    protected static final int FIELD_TYPE_NULL = 0;
    protected static final int FIELD_TYPE_STRING = 3;
    private static boolean positionFixed;

    public static int getColor(Context context, int id) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static void setTextAppearance(TextView textView, Context context, int textAppearance) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            textView.setTextAppearance(context, android.R.style.TextAppearance_Medium);
        } else {
            textView.setTextAppearance(android.R.style.TextAppearance_Medium);
        }

    }

    public static void setLocale(Configuration configuration, Locale locale) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale);
        } else {
            configuration.locale = locale;
        }
    }

    public static int getType(Cursor cursor, int i) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return cursor.getType(i);
        } else {
            SQLiteCursor sqLiteCursor = (SQLiteCursor) cursor;
            CursorWindow cursorWindow = sqLiteCursor.getWindow();
            int pos = cursor.getPosition();
            int type = -1;
            if (cursorWindow.isNull(pos, i)) {
                type = FIELD_TYPE_NULL;
            } else if (cursorWindow.isLong(pos, i)) {
                type = FIELD_TYPE_INTEGER;
            } else if (cursorWindow.isFloat(pos, i)) {
                type = FIELD_TYPE_FLOAT;
            } else if (cursorWindow.isString(pos, i)) {
                type = FIELD_TYPE_STRING;
            } else if (cursorWindow.isBlob(pos, i)) {
                type = FIELD_TYPE_BLOB;
            }
            return type;
        }
    }

    public static void recreate(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            activity.recreate();
        } else {
            Intent intent = activity.getIntent();
            activity.finish();
            activity.startActivity(intent);
        }
    }

    public static void smoothScrollToPositionFromTop(final AbsListView view, final int position, final int offset, final int duration) {
        if (Build.VERSION.SDK_INT >= 11) {
            view.smoothScrollToPositionFromTop(position, offset, duration);
        } else {
            view.setSelection(position);
        }

    }

    public static View getChildAtPosition(final AdapterView view, final int position) {
        final int index = position - view.getFirstVisiblePosition();
        if ((index >= 0) && (index < view.getChildCount())) {
            return view.getChildAt(index);
        } else {
            return null;
        }
    }

    public static void speak(final TextToSpeech tts, String text, int queueMode) {
        String utteranceId = "UniqueID";
        if (Build.VERSION.SDK_INT >= 21) {
            tts.speak(text, queueMode, null, utteranceId);
        } else {
            HashMap<String, String> map = new HashMap<>();
            map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);
            tts.speak(text, queueMode, map);
        }

    }

}
