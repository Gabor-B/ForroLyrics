package de.gabbo.forro_lyrics;

import android.content.Intent;
import android.content.res.AssetManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import de.gabbo.forro_lyrics.activities.MainActivity;
import de.gabbo.forro_lyrics.activities.SplashActivity;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;
import de.gabbo.forro_lyrics.sql.SQLiteDefines;

/**
 * Created by Gabor on 22.07.2016.
 * Initialization task called by the splash screen
 */
public class InitTask implements Runnable {

    private SplashActivity splashActivity;
    private static String TAG = InitTask.class.getSimpleName();

    public InitTask(SplashActivity splashActivity) {
        this.splashActivity = splashActivity;
    }

    @Override
    public void run() {
        Log.d(TAG, "Start splash");
        String storagePath = splashActivity.getFilesDir().getParentFile() + "/databases";
        File outFile = new File(storagePath, SQLiteDefines.NAME_DB);
        //Log.d(TAG, "Outdir exists: " + outFile.getParentFile().exists() + "| created: " + outFile.getParentFile().mkdirs());
        //Log.d(TAG, "Outfile exists: " + outFile.exists());
        if (!outFile.exists()) {
            copyDatabaseFile(splashActivity.getAssets(), outFile);
        }
        readSQLDatabase(outFile);
        Intent intent = new Intent(splashActivity, MainActivity.class);
        splashActivity.startActivity(intent);
        //Log.d(TAG, "Stop splash");
        splashActivity.finish();
    }

    /**
     * Open and init the SQL database
     */
    private void readSQLDatabase(File outFile) {
        SQLiteDataSource dataSource = SQLiteDataSource.getInstance(splashActivity);
        dataSource.open();
        Log.d(TAG, "Needs update: " + dataSource.needsUpgrade());
        if (dataSource.needsUpgrade()) {
            dataSource.close();
            boolean result = outFile.delete();
            Log.d(TAG, "DB file deleted: " + result + ", dir exists: " + outFile.getParentFile().exists());
            copyDatabaseFile(splashActivity.getAssets(), outFile);
            dataSource.open();
            Toast.makeText(splashActivity, R.string.text_replace_database, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Copy the SQL database from the db or zip file to the app directory
     * @param assetManager  apps assetmanager
     * @param outFile   File object for the DB output file
     */
    public static void copyDatabaseFile(AssetManager assetManager, File outFile) {
        List<String> assets;
        try {
            boolean dirCreated = outFile.getParentFile().mkdirs();
            Log.d(TAG, "Found db dir exists: " + dirCreated + ", db file exists: " + outFile.exists());
            assets = Arrays.asList(assetManager.list(""));
            if (assets.contains(SQLiteDefines.NAME_DB)) {
                Log.d(TAG, "Found db file");
                copyDatabase(assetManager.open(SQLiteDefines.NAME_DB), outFile);
                return;
            } else if (assets.contains(SQLiteDefines.NAME_DB_ZIP)) {
                Log.d(TAG, "Found zip db file");
                copyZipDatabase(assetManager.open(SQLiteDefines.NAME_DB_ZIP), outFile);
                return;
            }
            Log.e(TAG, "No lyrics asset file found!");
        } catch (IOException e) {
            Log.e(TAG, "Error reading the assets");
        }
    }

    private static void copyDatabase(InputStream in, File outFile) {
        try {
            OutputStream out = new FileOutputStream(outFile);
            Log.d(TAG, "asset copy to: " + outFile);
            byte[] buffer = new byte[1024];
            int read;
            while((read = in.read(buffer)) != -1){
                out.write(buffer, 0, read);
            }
            in.close();
            out.flush();
            out.close();
        } catch(IOException e) {
            Log.e(TAG, "Failed to copy asset file: " + SQLiteDefines.NAME_DB, e);
        }
    }

    private static void copyZipDatabase(InputStream in, File outFile) {
        try {
            FileOutputStream fout = new FileOutputStream(outFile);
            String filename;
            //String path = outFile.getParent();
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(in));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;
            while ((ze = zis.getNextEntry()) != null)
            {
                filename = ze.getName();
                /*if (ze.isDirectory()) {
                    File fmd = new File(path + filename);
                    fmd.mkdirs();
                    continue;
                }*/
                if (filename.equals(outFile.getName())) {
                    while ((count = zis.read(buffer)) != -1) {
                        fout.write(buffer, 0, count);
                    }
                }
                fout.close();
                zis.closeEntry();
            }
            zis.close();
        } catch(IOException e) {
            Log.e(TAG, "Failed to copy asset file from zip: " + SQLiteDefines.NAME_DB, e);
        }
    }
}