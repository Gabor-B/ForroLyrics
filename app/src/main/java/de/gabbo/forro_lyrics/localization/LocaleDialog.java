package de.gabbo.forro_lyrics.localization;

import android.content.DialogInterface;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.gabbo.forro_lyrics.CompatibilityWrapper;
import de.gabbo.forro_lyrics.R;

/**
 * Created by Gabor on 08.08.2016.
 * Class for the language change selection dialog window
 */
public class LocaleDialog {

    private final AlertDialog.Builder builder;

    public LocaleDialog(final AppCompatActivity activity) {
        builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.action_set_language));
        builder.setItems(R.array.array_translation_lang, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    SettingsHelper.setLocale(activity.getBaseContext(), "de");
                    CompatibilityWrapper.recreate(activity);
                    //activity.recreate();
                } else if (which == 1) {
                    SettingsHelper.setLocale(activity.getBaseContext(), "en");
                    CompatibilityWrapper.recreate(activity);
                    //activity.recreate();
                } else if (which == 2) {
                    SettingsHelper.setLocale(activity.getBaseContext(), "pt");
                    CompatibilityWrapper.recreate(activity);
                    //activity.recreate();
                } else {
                    Toast.makeText(activity, activity.getString(R.string.text_unknown_lang_selected), Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
    }

    /**
     * Create the language change dialog
     */
    public void createDialog() {
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
