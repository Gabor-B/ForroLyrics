package de.gabbo.forro_lyrics.localization;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import java.util.Locale;

import de.gabbo.forro_lyrics.CompatibilityWrapper;
import de.gabbo.forro_lyrics.R;

/**
 * This class is used to change the application settings and persist this change for the next time
 * that your app is going to be used.
 * <p/>
 * You can also change the locale of your application on the fly by using the setLocale method.
 * <p/>
 */
public class SettingsHelper {

    private static final String SELECTED_LANGUAGE = "Settings.Helper.Selected.Language";
    private static final String SELECTED_LIGHTS_ON = "Settings.Helper.Selected.LightsOn";
    private static final String SELECTED_DARK = "Settings.Helper.Selected.DarkThemeSet";
    private static final String TAG = SettingsHelper.class.getSimpleName();

    /**
     * @param context   source context
     * @return  get current language
     */
    public static String getLanguage(Context context) {
        return getPersistedLocaleData(context, Locale.getDefault().getLanguage());
    }

    /**
     * Set language
     * @param context       source context
     * @param language      set app language
     */
    public static void setLocale(Context context, String language) {
        persistLocale(context, language);
        updateResources(context, language);
    }

    public static void setDarkThemeOn(Context context, boolean darkThemeOn) {
        persistDarkTheme(context, darkThemeOn);
    }

    public static void setDarkThemeInApp(Context context) {
        if (SettingsHelper.isDarkThemeOn(context)) {
            context.setTheme(R.style.AppThemeDark);
        } else {
            context.setTheme(R.style.AppTheme);
        }
    }

    public static boolean isDarkThemeOn(Context context) {
        return getPersistedDarkThemeOnData(context);
    }

    public static void setLightsAlwaysOn(Context context, boolean lightsAlwaysOn) {
        persistLightsOn(context, lightsAlwaysOn);
    }

    public static boolean isLightsAlwaysOn(Context context) {
        return getPersistedLightsOnData(context);
    }

    private static String getPersistedLocaleData(Context context, String defaultLanguage) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        //Log.d(TAG, preferences.getString(SELECTED_LANGUAGE, defaultLanguage));
        return preferences.getString(SELECTED_LANGUAGE, defaultLanguage);
    }

    private static Boolean getPersistedLightsOnData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        //Log.d(TAG, "Lights on setting: " + preferences.getBoolean(SELECTED_LIGHTS_ON, false));
        return preferences.getBoolean(SELECTED_LIGHTS_ON, false);
    }

    private static Boolean getPersistedDarkThemeOnData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        //Log.d(TAG, "Dark theme setting: " + preferences.getBoolean(SELECTED_DARK, false));
        return preferences.getBoolean(SELECTED_DARK, false);
    }

    private static void persistLocale(Context context, String language) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SELECTED_LANGUAGE, language);
        editor.apply();
    }

    private static void persistDarkTheme(Context context, boolean isThemeDarkSet) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SELECTED_DARK, isThemeDarkSet);
        editor.apply();
    }

    private static void persistLightsOn(Context context, boolean lightsOn) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SELECTED_LIGHTS_ON, lightsOn);
        editor.apply();
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        CompatibilityWrapper.setLocale(configuration, locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

}
