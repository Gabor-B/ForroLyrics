package de.gabbo.forro_lyrics.localization;

import android.content.res.AssetManager;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Gabor on 08.08.2016.
 * Class for the changelog display dialog window
 */
public class ChangelogDialog {

    private static final String TAG = ChangelogDialog.class.getSimpleName();
    private final AlertDialog.Builder builder;
    private final AssetManager assets;

    public ChangelogDialog(final AppCompatActivity activity) {
        builder = new AlertDialog.Builder(activity);
        builder.setNeutralButton(android.R.string.ok, null);
        this.assets = activity.getAssets();
    }

    /**
     * Create the changelog dialog
     */
    public void createDialog() {
        String message = "";
        try {
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(assets.open("CHANGELOG")));
            String line;
            while ((line = reader.readLine()) != null) {
                message += line + "\n";
            }
        } catch (IOException e) {
            Log.e(TAG, "error opening changelog");
        }
        builder.setMessage(message);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
