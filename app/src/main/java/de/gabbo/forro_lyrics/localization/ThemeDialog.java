package de.gabbo.forro_lyrics.localization;

import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.gabbo.forro_lyrics.CompatibilityWrapper;
import de.gabbo.forro_lyrics.R;

/**
 * Created by Gabor on 08.08.2016.
 * Class for the dark/light theme selection dialog window
 */
public class ThemeDialog {

    private final AlertDialog.Builder builder;

    public ThemeDialog(final AppCompatActivity activity) {
        builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.action_set_theme));
        builder.setItems(R.array.array_theme_selection, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    SettingsHelper.setDarkThemeOn(activity.getBaseContext(), false);
                    CompatibilityWrapper.recreate(activity);
                } else {
                    SettingsHelper.setDarkThemeOn(activity.getBaseContext(), true);
                    CompatibilityWrapper.recreate(activity);
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
    }

    /**
     * Create the language change dialog
     */
    public void createDialog() {
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
