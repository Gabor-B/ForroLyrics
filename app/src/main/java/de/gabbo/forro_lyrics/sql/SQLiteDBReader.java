package de.gabbo.forro_lyrics.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Gabor on 18.07.2016.
 * This class handles the (direct) communication to the database
 */
public class SQLiteDBReader extends SQLiteOpenHelper {

    private static final String TAG = SQLiteDBReader.class.getSimpleName();

    public static final String SQL_DROP = "DROP TABLE IF EXISTS " + SQLiteDefines.TABLE_NAME;
    private boolean needsUpgrade = false;

    /**
     * Get the column number for a database column
     * @param columnId  column identifier
     * @return  column number
     */
    public static int getIndex(String columnId) {
        switch(columnId) {
        case SQLiteDefines.COLUMN_ARTIST:
            return 1;
        case SQLiteDefines.COLUMN_LYRIC:
            return 5;
        case SQLiteDefines.COLUMN_LANGUAGE:
            return 4;
        case SQLiteDefines.COLUMN_TRACK:
            return 3;
        case SQLiteDefines.COLUMN_ALBUM:
            return 2;
        default:
            return 0;
        }
    }

    public SQLiteDBReader(Context context) {
        super(context, SQLiteDefines.NAME_DB, null, SQLiteDefines.DB_VERSION);
        Log.d(TAG, "DbReader created the database: " + "getDatabaseName()");
        /*
        new SQLiteDatabase.CursorFactory() {
            @Override
            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver masterQuery, String editTable, SQLiteQuery query) {
                Log.d(TAG, "SQL query: " + query.toString());
                return new SQLiteCursor(db, masterQuery, editTable, query);
            }
        }
         */
    }

    // Die onCreate-Methode wird nur aufgerufen, falls die Datenbank noch nicht existiert
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*try {
            Log.d(TAG, "Creating DB");
            db.execSQL(SQLiteDefines.TABLE_CREATE);
        }
        catch (Exception ex) {
            Log.e(TAG, "Error creating DB: " + ex.getMessage());
        }*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "Old DB with version " + oldVersion + " will be removed. New DB will be created with version " + newVersion);
        db.execSQL(SQL_DROP);
        needsUpgrade = true;
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "Newer DB found with version " + newVersion + ", no idea what to do with old version: " + oldVersion);
    }

    public boolean needsUpgrade() {
        return needsUpgrade;
    }
}
