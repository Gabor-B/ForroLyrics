package de.gabbo.forro_lyrics.sql.providers;

import android.database.Cursor;

import de.gabbo.forro_lyrics.listindexer.ListDataInterface;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;

/**
 * Created by Gabor on 24.07.2016.
 * Interface for the search by track function
 */
public class TrackDataProvider implements ListDataInterface {

    public static String NAME_ID = "provider_title";

    private SQLiteDataSource dataSource;

    public TrackDataProvider(SQLiteDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public int getContentIndex() {
        return 3;
    }

    @Override
    public Cursor getDataCursor() {
        return dataSource.getAllTracks();
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof TrackDataProvider) || super.equals(obj);
    }

    @Override
    public int hashCode() {
        return NAME_ID.hashCode();
    }
}