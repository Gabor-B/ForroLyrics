package de.gabbo.forro_lyrics.sql;

/**
 * Created by Gabor on 09.08.2016.
 * Defines for the forro lyrics database
 */
public class SQLiteDefines {
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_LYRIC = "LYRIC";
    public static final String COLUMN_LANGUAGE = "LANGUAGE";
    public static final String COLUMN_TRACK = "TRACK";
    public static final String COLUMN_ALBUM = "ALBUM";
    public static final String COLUMN_ARTIST = "ARTIST";
    public static final String NAME_DB = "lyrics.db";
    public static final String NAME_DB_ZIP = "lyrics.zip";
    public static final String TABLE_NAME = "forro_lyrics";

    public static int DB_VERSION = 18;
    public static final String TABLE_VERSION = "PRAGMA user_version ('" + DB_VERSION + "')";

    public static final String[] COLUMNS = {
            COLUMN_ID,
            COLUMN_ARTIST,
            COLUMN_ALBUM,
            COLUMN_TRACK,
            COLUMN_LANGUAGE,
            COLUMN_LYRIC
    };

    public static final String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + " " + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_ARTIST + " TEXT NOT NULL, " +
            COLUMN_ALBUM + " TEXT NOT NULL, " +
            COLUMN_TRACK + " TEXT NOT NULL, " +
            COLUMN_LANGUAGE + " INTEGER NOT NULL," +
            COLUMN_LYRIC + " TEXT NOT NULL)";

}
