package de.gabbo.forro_lyrics.sql.providers;

import android.database.Cursor;

import java.util.ArrayList;

import de.gabbo.forro_lyrics.listindexer.ListDataInterface;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;

/**
 * Created by Gabor on 09.08.2016.
 * Interface for the search by track contatinng the searched string function
 */
public class SearchedTrackDataProvider implements ListDataInterface {

    public static String NAME_ID = "provider_search";

    private SQLiteDataSource dataSource;
    private ArrayList<String> entriesList;

    /**
     * @param dataSource    SQL handler instance
     * @param entriesList   search field entries
     */
    public SearchedTrackDataProvider(SQLiteDataSource dataSource, ArrayList<String> entriesList) {
        this.dataSource = dataSource;
        this.entriesList = entriesList;
    }

    @Override
    public int getContentIndex() {
        return 3;
    }

    @Override
    public Cursor getDataCursor() {
        return dataSource.getTracksContaining(entriesList);
    }
}
