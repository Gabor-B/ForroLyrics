package de.gabbo.forro_lyrics.sql.providers;

import android.database.Cursor;

import de.gabbo.forro_lyrics.listindexer.ListDataInterface;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;

/**
 * Created by Gabor on 26.07.2016.
 * Interface for the search by track from artist function
 */
public class SimilarTrackDataProvider  implements ListDataInterface {

    public static String NAME_ID = "provider_similartrack_titles";
    public static String TRACK_ID = "provider_similartrack_id";

    private String trackName;
    private SQLiteDataSource dataSource;

    public SimilarTrackDataProvider(SQLiteDataSource dataSource, String trackName) {
        this.dataSource = dataSource;
        this.trackName = trackName;
    }

    @Override
    public int getContentIndex() {
        return 3;
    }

    @Override
    public Cursor getDataCursor() {
        return dataSource.getTracksByName(trackName);
    }
}
