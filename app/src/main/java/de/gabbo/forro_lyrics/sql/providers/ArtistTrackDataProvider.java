package de.gabbo.forro_lyrics.sql.providers;

import android.database.Cursor;

import de.gabbo.forro_lyrics.listindexer.ListDataInterface;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;

/**
 * Created by Gabor on 26.07.2016.
 * Interface for the search by track from artist function
 */
public class ArtistTrackDataProvider  implements ListDataInterface {

    public static String NAME_ID = "provider_artists_titles";
    public static String ARTIST_ID = "provider_artists_id";
    public static int INDEX_ID = 2;

    private String artistName;
    private SQLiteDataSource dataSource;

    public ArtistTrackDataProvider(SQLiteDataSource dataSource, String artistName) {
        this.dataSource = dataSource;
        this.artistName = artistName;
    }

    @Override
    public int getContentIndex() {
        return 3;
    }

    @Override
    public Cursor getDataCursor() {
        return dataSource.getAllTracks(artistName);
    }

    @Override
    public int hashCode() {
        return artistName.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ArtistTrackDataProvider) {
            return artistName.equals(((ArtistTrackDataProvider) obj).artistName);
        }
        return super.equals(obj);
    }
}
