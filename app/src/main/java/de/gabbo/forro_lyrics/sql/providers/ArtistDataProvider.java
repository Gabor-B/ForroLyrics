package de.gabbo.forro_lyrics.sql.providers;

import android.database.Cursor;

import de.gabbo.forro_lyrics.listindexer.ListDataInterface;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;

/**
 * Created by Gabor on 24.07.2016.
 * Interface for the search by artist function
 */
public class ArtistDataProvider implements ListDataInterface {

    public static String NAME_ID = "provider_artist";

    private SQLiteDataSource dataSource;

    public ArtistDataProvider(SQLiteDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public int getContentIndex() {
        return 1;
    }

    @Override
    public Cursor getDataCursor() {
        return dataSource.getAllArtists();
    }

    @Override
    public int hashCode() {
        return NAME_ID.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof ArtistDataProvider) || super.equals(obj);
    }
}
