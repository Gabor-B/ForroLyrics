package de.gabbo.forro_lyrics.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.database.Cursor;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by Gabor on 12.07.2016.
 * This class provides abstracted communication to the database
 */
public class SQLiteDataSource implements Serializable {

    private static final String LOG_TAG = SQLiteDataSource.class.getSimpleName();

    private SQLiteDatabase database;
    private SQLiteDBReader dbHelper;
    private static SQLiteDataSource sInstance;

    /**
     * Get an instance of the database access class
     * @param context   source context
     * @return  the SQLiteDataSource class instance
     */
    public static synchronized SQLiteDataSource getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SQLiteDataSource(context.getApplicationContext());
        }
        return sInstance;
    }

    private SQLiteDataSource(Context context) {
        dbHelper = new SQLiteDBReader(context);
    }

    /**
     * Open database for writing
     */
    public void open() {
        database = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "Database opened for write, database path: " + database.getPath());
    }

    /**
     * Close the opened database
     */
    public void close() {
        dbHelper.close();
        Log.d(LOG_TAG, "Database closed.");
    }

    public void getAllEntires(Set<String> artists, Set<String> tracks) {
        Cursor cursor = database.query(SQLiteDefines.TABLE_NAME,
                SQLiteDefines.COLUMNS, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            int idIndex = cursor.getColumnIndex(SQLiteDefines.COLUMN_ID);
            int idArtist = cursor.getColumnIndex(SQLiteDefines.COLUMN_ARTIST);
            int idAlbum = cursor.getColumnIndex(SQLiteDefines.COLUMN_ALBUM);
            int idTrack = cursor.getColumnIndex(SQLiteDefines.COLUMN_TRACK);
            int idLang = cursor.getColumnIndex(SQLiteDefines.COLUMN_LANGUAGE);
            int idLyric = cursor.getColumnIndex(SQLiteDefines.COLUMN_LYRIC);

            String artist = cursor.getString(idArtist);
            String album = cursor.getString(idAlbum);
            String track = cursor.getString(idTrack);
            String lyric = cursor.getString(idLyric);
            int language = cursor.getInt(idLang);
            long id = cursor.getLong(idIndex);

            Log.d(LOG_TAG, "ID: " + id + ", Inhalt: " + artist + album + track + lyric + language);
            artists.add(artist);
            tracks.add(track);
            cursor.moveToNext();
        }
        cursor.close();
    }

    /**
     * Add a new entry to the database
     * @param artist    artist name
     * @param album     album name
     * @param track     track name
     * @param language  lyrics language
     * @param lyrics    lyrics text
     */
    public void addTrack(String artist, String album, String track, int language, String lyrics) {
        ContentValues values = new ContentValues();
        values.put(SQLiteDefines.COLUMN_ARTIST, artist);
        values.put(SQLiteDefines.COLUMN_ALBUM, album);
        values.put(SQLiteDefines.COLUMN_TRACK, track);
        values.put(SQLiteDefines.COLUMN_LANGUAGE, language);
        values.put(SQLiteDefines.COLUMN_LYRIC, lyrics);
        //Log.d(LOG_TAG, "added values: " + artist + album + track + language);
        SQLiteDatabase writableDatabase = dbHelper.getWritableDatabase();
        writableDatabase.insert(SQLiteDefines.TABLE_NAME, null, values);
    }

    /**
     * Get all artists in the database, sorted by artist name
     * @return  Cursor to the search results
     */
    public Cursor getAllArtists() {
        try {
            return database.query(true, SQLiteDefines.TABLE_NAME, SQLiteDefines.COLUMNS, null, null, SQLiteDefines.COLUMN_ARTIST, null,
                    SQLiteDefines.COLUMN_ARTIST + " COLLATE UNICODE ASC", null);
        } catch (SQLException e) {
            return new EmptyCursor();
        }
    }

    /**
     * Get all tracks in the database (not unique, can contain duplicates). a;phabetically sorted by tack name
     * @return  Cursor to the search results
     */
    public Cursor getAllTracks() {
        //return database.query(false, SQLiteDefines.TABLE_NAME, columns, null, null, SQLiteDefines.COLUMN_TRACK, null, null, null);
        try {
            return database.query(SQLiteDefines.TABLE_NAME, SQLiteDefines.COLUMNS, null, null,
                null, null, SQLiteDefines.COLUMN_TRACK + " COLLATE UNICODE ASC", null);
        } catch (SQLException e) {
            return new EmptyCursor();
        }
    }

    public Cursor getSpecificTracks(String artist, String track) {
        try {
            return database.query(SQLiteDefines.TABLE_NAME, SQLiteDefines.COLUMNS, "(" + SQLiteDefines.COLUMN_ARTIST + "='" + artist
                        + "' COLLATE NOCASE) AND (" + SQLiteDefines.COLUMN_TRACK + "='" + track + "' COLLATE NOCASE)",
                null, null, null, null, null);
        } catch (SQLException e) {
            return new EmptyCursor();
        }
    }

    public Cursor getTracksByName(String trackName) {
        Log.d(LOG_TAG, "looking for track : " + trackName);
        try {
            return database.query(SQLiteDefines.TABLE_NAME, SQLiteDefines.COLUMNS, SQLiteDefines.COLUMN_TRACK + " LIKE ?",
                new String[]{trackName + "%"}, null, null, null, null);
        } catch (SQLException e) {
            return new EmptyCursor();
        }
    }

    /**
     * Get all tracks from a certain artist
     * @param artistName    Name of artist to search for
     * @return  Cursor to the search results
     */
    public Cursor getAllTracks(String artistName) {
        Log.d(LOG_TAG, "looking for all tracks from: " + artistName);
        try {
            return database.query(SQLiteDefines.TABLE_NAME, SQLiteDefines.COLUMNS, SQLiteDefines.COLUMN_ARTIST + " LIKE ?",
                    new String[]{artistName + "%"}, null, null, null, null);
        } catch (SQLException e) {
            return new EmptyCursor();
        }
    }

    /**
     * Get all tracks containing the text passages in the searchTerms, sorted by track names
     * @param searchTerms   whole words array to search for. For example "e voce" or "sanfona"
     * @return  Cursor to the search results
     */
    public Cursor getTracksContaining(Iterable<String> searchTerms) {
        String searchString = "";
        for (String searchTerm : searchTerms) {
            searchString += "(" + SQLiteDefines.COLUMN_LYRIC + " LIKE '%" +searchTerm + "%') AND ";
        }
        searchString = searchString.substring(0, searchString.length() - 4);    // remove last OR
        Log.d(LOG_TAG, "looking for : " + searchString);
        try {
            return database.query(SQLiteDefines.TABLE_NAME, SQLiteDefines.COLUMNS, searchString,
                    null, null, null, SQLiteDefines.COLUMN_TRACK + " COLLATE UNICODE ASC", null);
        } catch (SQLException e) {
            return new EmptyCursor();
        }
    }

    public boolean needsUpgrade() {
        return dbHelper.needsUpgrade();
    }

}