package de.gabbo.forro_lyrics;

import android.content.Context;
import android.util.Log;

import java.lang.reflect.Field;

/**
 * Created by gabor on 06.12.16.
 * This class provides often used functions to the developer
 */
public class Utilities {

    private static final String TAG = Utilities.class.getSimpleName();

    public static int getResourceId(String resourceName, Class<?> resourceClassName) {
        try {
            Field idField = resourceClassName.getDeclaredField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return -1;  // return invalid resource ID
    }

    public static int getStringIdentifier(Context context, String name) {
        return context.getResources().getIdentifier(name, "string", context.getPackageName());
    }
}
