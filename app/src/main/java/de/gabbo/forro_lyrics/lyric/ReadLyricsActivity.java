package de.gabbo.forro_lyrics.lyric;

import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

import de.gabbo.forro_lyrics.CompatibilityWrapper;
import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.Utilities;
import de.gabbo.forro_lyrics.localization.SettingsHelper;
import de.gabbo.forro_lyrics.representation.LangEnum;

/**
 * This activity reads out the lyrics aloud
 * Created by Gabor on 27.12.2016.
 */
public class ReadLyricsActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    private static final String TAG = ReadLyricsActivity.class.getSimpleName();
    private TextToSpeech tts;
    private TextView txtArtist;
    private TextView txtLyrics;
    private LangEnum language;
    private Button btnSpeakLyrics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsHelper.setDarkThemeInApp(this);
        setContentView(R.layout.activity_read_lyrics);
        Bundle extras = getIntent().getExtras();
        txtLyrics = (TextView) findViewById(R.id.lyrics_field);
        if (extras != null) {
            String artistName = extras.getString(getString(R.string.artist));
            String albumName = extras.getString(getString(R.string.album));
            String trackName = extras.getString(getString(R.string.track));
            String lyrics = extras.getString(getString(R.string.lyric));
            int langId = extras.getInt(getString(R.string.language));
            language = LangEnum.values()[langId];
            txtArtist = (TextView) findViewById(R.id.artist_track_field);
            String titleString;
            if (artistName != null && artistName.equals(albumName)) {
                titleString = artistName + " - " + trackName;
            } else {
                titleString = artistName + " - " + albumName + " - " + trackName;
            }
            txtArtist.setText(titleString);
            txtLyrics.setText(lyrics);
        }
        tts = new TextToSpeech(this, this);
        ImageButton btnSpeakTitle = (ImageButton) findViewById(R.id.button_read_artist_play);
        btnSpeakLyrics = (Button) findViewById(R.id.button_read_lyrics_play);
        btnSpeakTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                CompatibilityWrapper.speak(tts, txtArtist.getText().toString(), TextToSpeech.QUEUE_FLUSH);
            }
        });
        btnSpeakLyrics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (tts.isSpeaking()) {
                    tts.stop();
                    btnSpeakLyrics.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.ic_media_play, 0);
                } else {
                    btnSpeakLyrics.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.ic_media_pause, 0);
                    int selectionStart = txtLyrics.getSelectionStart();
                    int selectionEnd = txtLyrics.getSelectionEnd();
                    String text = txtLyrics.getText().toString();
                    if (selectionStart != selectionEnd) {   //both -1 or both at the same pos
                        text = text.substring(selectionStart, selectionEnd);
                    }
                    CompatibilityWrapper.speak(tts, text, TextToSpeech.QUEUE_FLUSH);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            checkLanguageSupport(language);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override public void onStart(String s) { }
                    @Override public void onError(String s) { }

                    @Override
                    public void onDone(String s) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnSpeakLyrics.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.ic_media_play, 0);
                            }
                        });
                    }
                });
            }
        } else {
            Log.e(TAG, "TTS initialization Failed!");
        }
    }

    private void checkLanguageSupport(LangEnum langId) {
        Locale locale;
        if (langId == LangEnum.DE) {
            locale = Locale.GERMAN;
        } else if (langId == LangEnum.FI) {
            locale = new Locale("fi");
        } else  if (langId == LangEnum.FR) {
            locale = Locale.FRENCH;
        } else  if (langId == LangEnum.PT) {
            locale = new Locale("pt");
        } else {
            locale = Locale.ENGLISH;
        }
        int result = tts.setLanguage(locale);
        if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            String langName = getString(Utilities.getStringIdentifier(getApplicationContext(), langId.getResourceId()));
            Toast.makeText(this, getString(R.string.error_tts_lang_missing) + langName, Toast.LENGTH_LONG).show();
        }
    }

}
