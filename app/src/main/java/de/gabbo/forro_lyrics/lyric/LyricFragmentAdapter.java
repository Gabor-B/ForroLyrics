package de.gabbo.forro_lyrics.lyric;

import android.content.Context;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.Utilities;
import de.gabbo.forro_lyrics.listindexer.list.ListEntry;
import de.gabbo.forro_lyrics.representation.LangEnum;

/**
 * This class handles the lyric language switching
 * Created by Gabor on 26.07.2016.
 */
public class LyricFragmentAdapter extends FragmentPagerAdapter {
    private static final String TAG = LyricFragmentAdapter.class.getSimpleName();

    private final ListEntry listEntry;
    private Context context;

    public LyricFragmentAdapter(FragmentManager fm, Context context, ListEntry listEntry) {
        super(fm);
        this.context = context;
        this.listEntry = listEntry;
    }

    @Override
    public int getCount() {
        return listEntry.getLyrics().size();
    }

    @Override
    public Fragment getItem(int position) {
        LyricFragment fragment = LyricFragment.newInstance(position);
        int langIndex = (int) listEntry.getLyrics().keySet().toArray()[position];
        Log.d(TAG, "getPosition: " + position + "| langindex: " + langIndex);
        fragment.setLyric(listEntry.getLyrics().get(langIndex), langIndex);
        fragment.setTrackData(listEntry.getArtistName(), listEntry.getAlbumName(), listEntry.getTrackName(), listEntry.getLyrics().keySet());
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        int language = (int) listEntry.getLyrics().keySet().toArray()[position];
        LangEnum langEnum = LangEnum.fromId(language);
        if (langEnum != null) {
            return context.getString(Utilities.getResourceId(langEnum.getResourceId(), R.string.class));
            //return langEnum.getLanguage();
        } else {
            return context.getString(R.string.lang_unkn);
        }
    }
}
