package de.gabbo.forro_lyrics.lyric;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Set;

import de.gabbo.forro_lyrics.activities.ContributeActivity;
import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.localization.SettingsHelper;

/**
 * This class is responsible for the lyrics text display for a single language
 * Created by Gabor on 26.07.2016.
 */
public class LyricFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private static final String TAG = LyricFragment.class.getSimpleName();

    //private int mPage;
    private String lyric;
    private String artist;
    private String album;
    private String track;
    private ArrayList<Integer> langIdsUsed = new ArrayList<>();
    private int currentLyricLang;

    public static LyricFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        LyricFragment fragment = new LyricFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //mPage = getArguments().getInt(ARG_PAGE);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lyric_fragment, container, false);
        TextView textView = (TextView) view.findViewById(R.id.lyric_view);
        //Log.d(TAG, "Lyric text: |" + lyric);
        textView.setText(lyric);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lyric, menu);
        boolean lightState = SettingsHelper.isLightsAlwaysOn(getActivity().getBaseContext());
        MenuItem item = menu.findItem(R.id.action_set_light_always_on);
        item.setChecked(lightState);
        setLightsOn(lightState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_report_error) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"forrolyrics@posteo.de"});
            i.putExtra(Intent.EXTRA_SUBJECT, "Report of an error in the lyrics");
            i.putExtra(Intent.EXTRA_TEXT   , "Error report for #" + artist + "#" + album + "#" + track + "#\n" +
                    "----------" + getString(R.string.report_hint_corrected_text) + "----------\n" + lyric + "\n" +
                    "----------" + getString(R.string.report_hint_original_text) + "----------\n" + lyric +
                    getString(R.string.feedback_terminate));
            try {
                startActivity(Intent.createChooser(i, getString(R.string.action_send_mail)));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getActivity(), getString(R.string.error_no_email), Toast.LENGTH_SHORT).show();
            }
            return true;
        } else if (id == R.id.action_contribute_translation) {
            Intent i = new Intent(getContext(), ContributeActivity.class);
            i.putExtra(getString(R.string.artist) , artist);
            i.putExtra(getString(R.string.album) , album);
            i.putExtra(getString(R.string.track) , track);
            i.putExtra(getString(R.string.language) , langIdsUsed);
            startActivity(i);
            return true;
        } else if (id == R.id.action_recontribute_lyric) {
            Intent i = new Intent(getContext(), ContributeActivity.class);
            i.putExtra(getString(R.string.artist) , artist);
            i.putExtra(getString(R.string.album) , album);
            i.putExtra(getString(R.string.track) , track);
            i.putExtra(getString(R.string.lyric) , lyric);
            i.putExtra(getString(R.string.language) , currentLyricLang);
            startActivity(i);
            return true;
        } else if (id == R.id.action_set_light_always_on) {
            boolean updatedState = !item.isChecked();
            item.setChecked(updatedState);
            SettingsHelper.setLightsAlwaysOn(getActivity().getBaseContext(), updatedState);
            setLightsOn(updatedState);
        } else if (id == R.id.action_read_lyric) {
            Intent i = new Intent(getContext(), ReadLyricsActivity.class);
            i.putExtra(getString(R.string.artist) , artist);
            i.putExtra(getString(R.string.album) , album);
            i.putExtra(getString(R.string.track) , track);
            i.putExtra(getString(R.string.lyric) , lyric);
            i.putExtra(getString(R.string.language) , currentLyricLang);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setLightsOn(boolean updatedState) {
        if (updatedState) {
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    public void setTrackData(String artistName, String albumName, String trackName, Set<Integer> langId) {
        this.album = albumName;
        this.artist = artistName;
        this.track = trackName;
        this.langIdsUsed.addAll(langId);
    }

    public void setLyric(String lyric, int langIndex) {
        this.currentLyricLang = langIndex;
        this.lyric = "";
        for (String lyricLine : lyric.split("\\|")) {
            this.lyric += (lyricLine.trim() + "\n");
            //Log.d(TAG, lyricLine.trim());
        }
    }

}