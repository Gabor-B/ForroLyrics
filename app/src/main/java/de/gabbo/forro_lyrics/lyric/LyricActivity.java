package de.gabbo.forro_lyrics.lyric;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.listindexer.list.ListEntry;
import de.gabbo.forro_lyrics.localization.SettingsHelper;

/**
 * This class realizes the activity for the lyrics page
 * Created by Gabor on 26.07.2016.
 */
public class LyricActivity extends AppCompatActivity {

    public static final String ENTRY_NAME = "entry_name";
    private static final String TAG = LyricActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsHelper.setDarkThemeInApp(this);
        setContentView(R.layout.activity_lyric);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        ListEntry listEntry = (ListEntry) getIntent().getExtras().getSerializable(ENTRY_NAME);
        if (listEntry != null) {
            viewPager.setAdapter(new LyricFragmentAdapter(getSupportFragmentManager(),
                    LyricActivity.this, listEntry));
            setTitle(listEntry.getArtistName() + " - " + listEntry.getTrackName());
            // Give the TabLayout the ViewPager
            TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

}
