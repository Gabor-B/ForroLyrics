package de.gabbo.forro_lyrics.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;

/**
 * This class implements a service that will listen to tracks played on the phone by the media player
 * Created by gabor on 11.10.16.
 */
public class PlayedTrackReceiverService extends Service implements PlayedTrackReceiverInterface {

    public static final String TAG = PlayedTrackReceiverService.class.getSimpleName();

    ArrayList<Messenger> mClients = new ArrayList<>(); // Keeps track of all current registered clients.
    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final String MSG_VALUE_ID = "value";
    final Messenger mMessenger = new Messenger(new IncomingHandler()); // Target we publish for clients to send messages to IncomingHandler.
    private PlayedTrackReceiver receiver;

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @SuppressWarnings({"ConstantConditions", "unchecked"})
    @Override
    public void sendMessageToUI(ValueEnum valueType, Object value) {
        for (int i=mClients.size()-1; i>=0; i--) {
            try {
                Bundle b = new Bundle();
                Message msg;
                if (valueType.equals(ValueEnum.PLAYING)) {
                    b.putBoolean(MSG_VALUE_ID, (Boolean) value);
                    msg = Message.obtain(null, valueType.getId());
                } else {
                    b.putStringArrayList(MSG_VALUE_ID, (ArrayList<String>) value);
                    msg = Message.obtain(null, valueType.getId());
                }
                msg.setData(b);
                mClients.get(i).send(msg);
            }
            catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }
    }

    private void sendMessagesToUI(Messenger messenger) {
        Bundle bundle = new Bundle();
        Message msg;
        try {
            bundle.putBoolean(MSG_VALUE_ID, receiver.isPlaying());
            msg = Message.obtain(null, ValueEnum.PLAYING.getId());
            ArrayList<String> entries = receiver.getArtist();
            if (entries.size() > 0) {
                Log.d(TAG, "Sending init msg " + entries);
                bundle.putStringArrayList(MSG_VALUE_ID, entries);
                msg = Message.obtain(null, ValueEnum.ARTIST.getId());
            }
            msg.setData(bundle);
            messenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        receiver = new PlayedTrackReceiver((AudioManager) this.getSystemService(Context.AUDIO_SERVICE), this);
        registerReceiver(receiver, receiver.getFilter());
        Log.i(TAG, "Service Started.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Received start id " + startId + ": " + intent);
        return START_STICKY; // run until explicitly stopped.
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        Log.i(TAG, "Service Stopped.");
    }

    class IncomingHandler extends Handler { // Handler of incoming messages from clients.
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    if (msg.replyTo != null) {
                        Log.d(TAG, "Regisered PlayedTrack");
                        mClients.add(msg.replyTo);
                        sendMessagesToUI(msg.replyTo);
                    } else {
                        Log.d(TAG, "Registered Main");
                    }
                    break;
                case MSG_UNREGISTER_CLIENT:
                    if (msg.replyTo != null) {
                        Log.d(TAG, "UnRegistered Playedtrack");
                        mClients.remove(msg.replyTo);
                    } else {
                        Log.d(TAG, "UnRegistered Main");
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

}
