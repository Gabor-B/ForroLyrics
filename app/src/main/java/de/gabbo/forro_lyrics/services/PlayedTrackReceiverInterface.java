package de.gabbo.forro_lyrics.services;

/**
 * Interface for the played track receiver to send the gathered data
 * Created by gabor on 14.10.16.
 */
public interface PlayedTrackReceiverInterface {

    enum ValueEnum {

        ARTIST(0),
        PLAYING(1);

        private final int id;

        ValueEnum(int i) {
            id = i;
        }

        public int getId() {
            return id;
        }
    }

    void sendMessageToUI(ValueEnum valueType, Object value);

}
