package de.gabbo.forro_lyrics.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.util.Log;

import java.util.ArrayList;

/**
 * This class receives the broadcasted music metadata
 * Created by gabor on 14.10.16.
 */
public class PlayedTrackReceiver extends BroadcastReceiver {

    private String TAG = PlayedTrackReceiver.class.getSimpleName();
    private PlayedTrackReceiverInterface sendInterface;
    private boolean playing = false;
    private ArrayList<String> entries = new ArrayList<>();

    public PlayedTrackReceiver(AudioManager manager, PlayedTrackReceiverInterface sendInterface) {
        this.sendInterface = sendInterface;
    }

    public IntentFilter getFilter() {
        IntentFilter iF = new IntentFilter();
        iF.addAction("com.android.music.musicservicecommand");
        iF.addAction("org.lineageos.eleven.metachanged");
        iF.addAction("com.android.music.METACHANGED");
        iF.addAction("com.android.music.metachanged");
        iF.addAction("com.miui.player.metachanged");
        iF.addAction("com.htc.music.metachanged");
        iF.addAction("com.nullsoft.winamp.metachanged");
        iF.addAction("com.real.IMP.metachanged");
        iF.addAction("com.htc.music.metachanged");
        iF.addAction("fm.last.android.metachanged");
        iF.addAction("com.sec.android.app.music.metachanged");
        iF.addAction("com.nullsoft.winamp.metachanged");
        iF.addAction("com.amazon.mp3.metachanged");
        iF.addAction("com.miui.player.metachanged");
        iF.addAction("com.real.IMP.metachanged");
        iF.addAction("com.sonyericsson.music.metachanged");
        iF.addAction("com.rdio.android.metachanged");
        iF.addAction("com.samsung.sec.android.MusicPlayer.metachanged");
        iF.addAction("com.andrew.apollo.metachanged");
        //iF.addAction("com.android.music.playstatechanged");
        iF.addAction("com.android.music.updateprogress");
        iF.addAction("com.android.music.musicservicecommand");
        iF.addAction("com.android.music.playbackcomplete");
        //iF.addAction("com.android.music.queuechanged");
        return iF;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String action = intent.getAction();
            String cmd = intent.getStringExtra("command");
            String artist = intent.getStringExtra("artist");
            String album = intent.getStringExtra("album");
            String track = intent.getStringExtra("track");
            playing = playing || intent.getBooleanExtra("playing", false);
            Log.d(TAG, "Received " + cmd + " : " + action);
            Log.d(TAG, artist + " : " + album + " : " + track + " | " + playing);
            entries.clear();
            entries.add(artist);
            entries.add(album);
            entries.add(track);
            sendInterface.sendMessageToUI(PlayedTrackReceiverInterface.ValueEnum.ARTIST, entries);
            sendInterface.sendMessageToUI(PlayedTrackReceiverInterface.ValueEnum.PLAYING, playing);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public ArrayList<String> getArtist() {
        return entries;
    }

    public boolean isPlaying() {
        return playing;
    }
}
