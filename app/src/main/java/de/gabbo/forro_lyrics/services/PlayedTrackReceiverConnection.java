package de.gabbo.forro_lyrics.services;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

/**
 * This class manages the service connection
 * Created by gabor on 14.10.16.
 */
public class PlayedTrackReceiverConnection implements ServiceConnection {

    private Messenger messenger;
    private Messenger handler;

    public PlayedTrackReceiverConnection(Messenger handler) {
        this.handler = handler;
    }

    public void onServiceConnected(ComponentName className, IBinder service) {
        messenger = new Messenger(service);
        try {
            Message msg = Message.obtain(null, PlayedTrackReceiverService.MSG_REGISTER_CLIENT);
            msg.replyTo = handler;
            messenger.send(msg);
        }
        catch (RemoteException e) {
            // In this case the service has crashed before we could even do anything with it
        }
    }

    public void onServiceDisconnected(ComponentName className) {
        // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
        messenger = handler;
        //textStatus.setText("Disconnected.");
    }

    public Messenger getMessengerService() {
        return messenger;
    }

    public Messenger getHandler() {
        return handler;
    }
}
