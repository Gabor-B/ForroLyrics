package de.gabbo.forro_lyrics.listindexer.list;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.gabbo.forro_lyrics.listindexer.ListDataInterface;
import de.gabbo.forro_lyrics.sql.providers.ArtistDataProvider;
import de.gabbo.forro_lyrics.sql.providers.ArtistTrackDataProvider;
import de.gabbo.forro_lyrics.sql.providers.TrackDataProvider;

/**
 * Created by gabor on 01.10.16.
 * This class buffers the loaded data to speed up application run
 */
public class LoaderBuffer {

    private static final String TAG = LoaderBuffer.class.getSimpleName();
    private static LoaderBuffer instance = null;
    private Map<ListDataInterface, ForroCursorLoader> loaderMap;

    protected LoaderBuffer() {
        loaderMap = new HashMap<>();
    }

    public static LoaderBuffer getInstance() {
        if(instance == null) {
            instance = new LoaderBuffer();
        }
        return instance;
    }

    public ForroCursorLoader getLoader(Context context, ListDataInterface listDataInterface) {
        Log.d(TAG, "loaders count: " + loaderMap.size());
        if (loaderMap.containsKey(listDataInterface)) {
            //Log.d(TAG, "load stored loader for database query");
            return loaderMap.get(listDataInterface);
        } else {
            LyricsLoader loader = new LyricsLoader(context, listDataInterface);
            //Log.d(TAG, "create new loader for database query");
            loaderMap.put(listDataInterface, loader);
            return loader;
        }
    }

    public void setBufferLoader(ListDataInterface listDataInterface, Cursor data, List<ListEntry> listEntries) {
        if (listDataInterface instanceof TrackDataProvider ||
                listDataInterface instanceof ArtistDataProvider ||
                listDataInterface instanceof ArtistTrackDataProvider) {
            ForroCursorLoader loader = loaderMap.get(listDataInterface);
            if (loader instanceof LyricsLoader) {
                //Log.d(TAG, "set buffered result for database loader");
                loaderMap.put(listDataInterface, new LyricsBuffer(loader.getContext(), data, listEntries));
            }
        }
    }

}
