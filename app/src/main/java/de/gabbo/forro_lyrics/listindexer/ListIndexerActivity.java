package de.gabbo.forro_lyrics.listindexer;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.listindexer.list.ForroCursorLoader;
import de.gabbo.forro_lyrics.listindexer.list.ListAdapter;
import de.gabbo.forro_lyrics.listindexer.list.ListEntry;
import de.gabbo.forro_lyrics.listindexer.list.LoaderBuffer;
import de.gabbo.forro_lyrics.localization.SettingsHelper;
import de.gabbo.forro_lyrics.sql.providers.ArtistDataProvider;
import de.gabbo.forro_lyrics.sql.providers.ArtistTrackDataProvider;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;
import de.gabbo.forro_lyrics.sql.SQLiteDefines;
import de.gabbo.forro_lyrics.sql.providers.SearchedTrackDataProvider;
import de.gabbo.forro_lyrics.sql.providers.SimilarTrackDataProvider;
import de.gabbo.forro_lyrics.sql.providers.TrackDataProvider;

public class ListIndexerActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, LoaderManager.LoaderCallbacks<Cursor> {

    public static String LIST_TYPE_NAME = "list_type";
    public static String ALPHABET = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    protected static final String TAG = ListIndexerActivity.class.getSimpleName();

    private SpecialAlphabetIndexer mIndexer;
    private ListAdapter mAdapter;
    private ListDataInterface listDataInterface = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsHelper.setDarkThemeInApp(this);
        setContentView(R.layout.activity_indexlist);
        prepareListDataInterfaces();
        LoaderManager lm = getSupportLoaderManager();
        lm.initLoader(SQLiteDefines.DB_VERSION, null, this);
    }

    private void prepareListDataInterfaces() {
        String listType = getIntent().getExtras().getString(LIST_TYPE_NAME);
        if (listType == null) {
            Log.e(TAG, "Bad listsource initialization, null element");
        } else if (listType.equals(ArtistDataProvider.NAME_ID)) {
            listDataInterface = new ArtistDataProvider(SQLiteDataSource.getInstance(getApplicationContext()));
            setTitle(R.string.title_artist_list);
        } else if (listType.equals(TrackDataProvider.NAME_ID)) {
            listDataInterface = new TrackDataProvider(SQLiteDataSource.getInstance(getApplicationContext()));
            setTitle(R.string.title_track_list);
        } else if (listType.equals(SearchedTrackDataProvider.NAME_ID)) {
            ArrayList<String> searchList = getIntent().getStringArrayListExtra(SearchedTrackDataProvider.NAME_ID);
            Log.d(TAG, "search for : " + searchList);
            listDataInterface = new SearchedTrackDataProvider(SQLiteDataSource.getInstance(getApplicationContext()), searchList);
            setTitle(R.string.title_track_list);
        } else if (listType.equals(ArtistTrackDataProvider.NAME_ID)) {
            String artistName = getIntent().getExtras().getString(ArtistTrackDataProvider.ARTIST_ID);
            listDataInterface = new ArtistTrackDataProvider(SQLiteDataSource.getInstance(getApplicationContext()), artistName);
            setTitle(artistName);
        } else if (listType.equals(SimilarTrackDataProvider.NAME_ID)) {
            String trackName = getIntent().getExtras().getString(SimilarTrackDataProvider.TRACK_ID);
            listDataInterface = new SimilarTrackDataProvider(SQLiteDataSource.getInstance(getApplicationContext()), trackName);
            setTitle(R.string.title_track_list);
        } else {
            Log.e(TAG, "Unknown list data source");
        }
        Log.d(TAG, "listType : " + listDataInterface.getClass().getSimpleName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        //MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView mSearchView = (SearchView)  MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        mSearchView.setOnQueryTextListener(this);
        return true;
    }

    /**
     * On selecting action bar icons
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
        case R.id.action_search:
            // search action
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
       // mStatusView.setText("Query = " + newText);
        Log.i(TAG, "Action Search Query: " + newText);
        mAdapter.getFilter().filter(newText);
		return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //mStatusView.setText("Query = " + query + " : submitted");
    	Log.i(TAG, "Action Search Query submit: " + query);
        return false;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "DB length: " + listDataInterface.getDataCursor().getCount());
        LoaderBuffer loaderBuffer = LoaderBuffer.getInstance();
        return loaderBuffer.getLoader(getApplicationContext(), listDataInterface);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data.getCount() > 0) {
            List<ListEntry> listEntries = ((ForroCursorLoader) loader).getListEntries();
            mAdapter = new ListAdapter(this, listEntries);
            mIndexer = new SpecialAlphabetIndexer(this, R.layout.listentry_item, listEntries);
            mAdapter.setIndexer(mIndexer);
            LinearLayout indexerLayout = (LinearLayout) findViewById(R.id.indexer_layout);
            ListView listView = (ListView) findViewById(R.id.entries_list);
            ForroListListener listener = new ForroListListener(mIndexer, listDataInterface, listEntries, indexerLayout, listView, this);
            if(listEntries != null && listEntries.size() > 0) {
                listView.setAdapter(mAdapter);
                listView.setOnItemClickListener(listener);
            }
            indexerLayout.setOnTouchListener(listener);
            listView.setOnScrollListener(listener);
            LoaderBuffer.getInstance().setBufferLoader(listDataInterface, data, listEntries);
        } else {
            resetAdapters();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        resetAdapters();
    }

    private void resetAdapters() {
        mAdapter = null;
        mIndexer = null;
        LinearLayout indexerLayout = (LinearLayout) findViewById(R.id.indexer_layout);
        ListView listView = (ListView) findViewById(R.id.entries_list);
        listView.setAdapter(null);
        listView.setOnItemClickListener(null);
        indexerLayout.setOnTouchListener(null);
        listView.setOnScrollListener(null);
    }

}
