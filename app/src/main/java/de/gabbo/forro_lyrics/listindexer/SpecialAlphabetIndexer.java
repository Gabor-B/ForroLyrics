package de.gabbo.forro_lyrics.listindexer;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;

import java.text.Collator;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

import de.gabbo.forro_lyrics.listindexer.list.ListEntry;

/**
 * AlphabetIndexer class for the search lists
 * Created by Gabor on 26.07.2016.
 */
public class SpecialAlphabetIndexer extends ArrayAdapter<ListEntry> implements SectionIndexer {
    private static final String TAG = SpecialAlphabetIndexer.class.getSimpleName();
    private final Collator mCollator;
    private LinkedHashMap<String, Integer> mapIndex;
    private LinkedHashMap<Integer, Integer> mapIndexPosition;
    private LinkedHashMap<Integer, String> mapSections;
    private LinkedHashMap<Integer, Integer> mapSectionIndex;
    private String[] sections;

    public SpecialAlphabetIndexer(Context context, int textViewResourceId, List<ListEntry> objects) {
        super(context, textViewResourceId, objects);
        mCollator = java.text.Collator.getInstance();
        mCollator.setStrength(java.text.Collator.PRIMARY);
        buildIndex(objects);
    }

    private void buildIndex(List<ListEntry> objects) {
        mapIndex = new LinkedHashMap<>();
        mapIndexPosition = new LinkedHashMap<>();
        mapSectionIndex = new LinkedHashMap<>();
        mapSections = new LinkedHashMap<>();
        for (int x = 0; x < objects.size(); x++) {
            String sortKey = objects.get(x).getSortKey();
            String ch = deAccent(sortKey).substring(0, 1);
            ch = ch.toUpperCase(Locale.US);
            if (!mapIndex.containsKey(ch)) {
                mapIndex.put(ch, x);
            }
            mapSectionIndex.put(x, mapIndex.size() - 1);
        }
        Set<String> sectionLetters = mapIndex.keySet();
        int sectionPosition = 0;
        for (int index = 0; index < ListIndexerActivity.ALPHABET.length(); index++) {
            Character letter = ListIndexerActivity.ALPHABET.charAt(index);
            mapSections.put(index, sectionLetters.toArray()[sectionPosition].toString());
            //Log.d(TAG, "sections: " + index + "->" + sectionLetters.toArray()[sectionPosition].toString());
            if (sectionLetters.contains(letter.toString())) {
                //Log.d(TAG, "sections contains: " + letter);
                if (sectionPosition < sectionLetters.size() - 1) {
                    sectionPosition++;
                }
                mapIndexPosition.put(mapIndexPosition.size(), index);
            }
        }
        // create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<>(sectionLetters);
        Collections.sort(sectionList);
        sections = new String[sectionList.size()];
        sectionList.toArray(sections);
    }

    public Object[] getSections() {
        return sections;
    }

    public int getPositionForSectionSearch(int section) {
        //Log.d(TAG, "getPositionForSection " + section + "->" + mapSections.get(section) + "->" + mapIndex.get(mapSections.get(section)));
        return mapIndex.get(mapSections.get(section));
    }

    public int getPositionForSection(int section) {
        if (section >= sections.length) {
            return 0;
        }
        //Log.d(TAG, "getPositionForSection " + section + "->" + sections[section] + "->" + mapIndex.get(sections[section]));
        return mapIndex.get(sections[section]);
    }

    public int getSectionForPosition(int position) {
        //Log.d(TAG, "getSectionForPosition " + position + "->" + mapSectionIndex.get(position));
        return mapSectionIndex.get(position);
    }

    public int getSkippedIndex(int selectionIndex) {
        if (mapIndexPosition.containsKey(selectionIndex)) {
            return mapIndexPosition.get(selectionIndex);
        } else {
            return mapIndexPosition.get(mapIndexPosition.size()-1);
        }
    }

    protected int compare(String word, String letter) {
        String firstLetter;
        if (word.length() == 0) {
            firstLetter = " ";
        } else {
            firstLetter = word.substring(0, 1);
        }
        firstLetter = deAccent(firstLetter);
        return mCollator.compare(firstLetter, letter);
    }

    public static String deAccent(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

}
