package de.gabbo.forro_lyrics.listindexer.list;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.gabbo.forro_lyrics.listindexer.ListDataInterface;
import de.gabbo.forro_lyrics.listindexer.SpecialAlphabetIndexer;
import de.gabbo.forro_lyrics.sql.providers.ArtistTrackDataProvider;
import de.gabbo.forro_lyrics.sql.SQLiteDBReader;
import de.gabbo.forro_lyrics.sql.SQLiteDefines;

/**
 * This class loads the data from the lyrics database and builds the ListEntry container
 * Created by Gabor on 16.09.2016.
 */
public class LyricsLoader extends ForroCursorLoader {

    private static final String TAG = LyricsLoader.class.getSimpleName();
    private List<ListEntry> listEntries = new ArrayList<>();
    private ListDataInterface listDataInterface;

    public LyricsLoader(Context context, ListDataInterface listDataInterface) {
        super(context);
        this.listDataInterface = listDataInterface;
    }

    @Override
    public Cursor loadInBackground() {
        Log.d("Loader", "Getting data from DB");
        Cursor cursor = listDataInterface.getDataCursor();
        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(listDataInterface.getContentIndex());
                Integer language = cursor.getInt(SQLiteDBReader.getIndex(SQLiteDefines.COLUMN_LANGUAGE));
                String lyric = cursor.getString(SQLiteDBReader.getIndex(SQLiteDefines.COLUMN_LYRIC));
                ListEntry listEntry = new ListEntry();
                listEntry.setName(name);
                listEntry.setAlbumName(cursor.getString(SQLiteDBReader.getIndex(SQLiteDefines.COLUMN_ALBUM)));
                listEntry.setArtistName(cursor.getString(SQLiteDBReader.getIndex(SQLiteDefines.COLUMN_ARTIST)));
                listEntry.setTrackName(cursor.getString(SQLiteDBReader.getIndex(SQLiteDefines.COLUMN_TRACK)));
                listEntry.addLyric(language, lyric);
                if (listDataInterface instanceof ArtistTrackDataProvider) {
                    //Log.d(TAG, "Albumname: " + cursor.getString(ArtistTrackDataProvider.INDEX_ID));
                    listEntry.setSortKey(cursor.getString(ArtistTrackDataProvider.INDEX_ID));
                } else {
                    String key = SpecialAlphabetIndexer.deAccent(name.substring(0, 1).toUpperCase());
                    if (key.matches("[A-Z]")) {
                        listEntry.setSortKey(key);
                    } else {
                        listEntry.setSortKey("#");
                    }
                    //Log.d(TAG, "sortkey: " + key + "|" + name);
                }
                //Log.d(TAG, "listEntries contains " + listEntry + " | " + listEntries.contains(listEntry));
                if (listEntries.contains(listEntry)) {
                    for (ListEntry item : listEntries) {
                        if (item.compareTo(listEntry) == 0) {
                            item.addLyric(language, lyric);
                            //listEntries.add(placeholderListEntry);
                        }
                    }
                } else {
                    listEntries.add(listEntry);
                }
                //Log.d(TAG, "sortKey from cursor " + listEntry);
            } while (cursor.moveToNext());
        }
        Log.d(TAG, "ListEntries count: " + listEntries + "| " + listEntries.size());
        //return newCursor;
        return cursor;
    }

    public List<ListEntry> getListEntries() {
        return listEntries;
    }
}
