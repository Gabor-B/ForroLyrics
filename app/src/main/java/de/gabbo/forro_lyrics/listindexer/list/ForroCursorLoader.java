package de.gabbo.forro_lyrics.listindexer.list;

import android.content.Context;

import java.util.List;

import de.gabbo.forro_lyrics.sql.SimpleCursorLoader;

/**
 * Created by gabor on 01.10.16.
 * Abstract class for loading the lyrics data
 */
public abstract class ForroCursorLoader extends SimpleCursorLoader {

    public ForroCursorLoader(Context context) {
        super(context);
    }

    /**
     * @return the ListEntry objects built from the DB query result
     */
    public abstract List<ListEntry> getListEntries();

}
