package de.gabbo.forro_lyrics.listindexer.list;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import de.gabbo.forro_lyrics.R;

public class ListAdapter extends BaseAdapter implements Filterable {

    private static final String TAG = ListAdapter.class.getSimpleName();
    private List<ListEntry> listEntries;
    /** This is mainly for listview search **/
    private List<ListEntry> listForSearch;
    private LayoutInflater mInflater;
    private SectionIndexer mIndexer;
    
    public ListAdapter(Context context, List<ListEntry> listEntries) {
        super();
        this.listEntries = listEntries;
        listForSearch = listEntries;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listEntries.size();
    }

    @Override
    public Object getItem(int position) {
        return listEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        ListEntry listEntry = listEntries.get(position);
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.listentry_item, null);
            holder = new ViewHolder();
            holder.sortKeyLayout = (LinearLayout) convertView.findViewById(R.id.sort_key_layout);
            holder.sortKey = (TextView) convertView.findViewById(R.id.sort_key);
            holder.listEntry = (TextView) convertView.findViewById(R.id.listentry_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //Log.d(TAG, "listentry: " + listEntry.getName());
        holder.listEntry.setText(listEntry.getName());
        int section = mIndexer.getSectionForPosition(position);
        if (position == mIndexer.getPositionForSection(section)) {
            String sortKeyText = listEntry.getSortKey();
            //Log.d(TAG, "Setting sortkey text on screen: " + sortKeyText + "| pos " + position + "| sec: " + section + "| posForSec: " + mIndexer.getPositionForSection(section));
            holder.sortKey.setText(sortKeyText);
            holder.sortKeyLayout.setVisibility(View.VISIBLE);
        } else {
            //Log.d(TAG, "No sortkey on pos " + position + "| sec: " + section + "| posForSec: " + mIndexer.getPositionForSection(section));
            holder.sortKeyLayout.setVisibility(View.GONE);
        }
        return convertView;
    }

    /**
     * @param indexer   used indexer
     */
    public void setIndexer(SectionIndexer indexer) {
        mIndexer = indexer;
    }
    
    private static class ViewHolder {
        LinearLayout sortKeyLayout;
        TextView sortKey;
        TextView listEntry;
    }

	@Override
	public Filter getFilter() {
		return myFilter;
	}
    
    Filter myFilter = new Filter() {
		
		@SuppressWarnings("unchecked")
		@Override
		public void publishResults(CharSequence constraint, FilterResults results) {
			listEntries = (List<ListEntry>) results.values;
	          if (results.count > 0) {
	           notifyDataSetChanged();
	          } else {
	              notifyDataSetInvalidated();
	          }
		}
		
		@Override
		public FilterResults performFiltering(CharSequence constraint) {
			FilterResults filterResults = new FilterResults();   
	         ArrayList<ListEntry> tempGlossaryList=new ArrayList<>();

	         if(constraint != null && listForSearch !=null) {
	             int length= listForSearch.size();
	             Log.i("Filtering", "glossaries size"+length);
	             int i=0;
	                while(i<length){
                        ListEntry item= listForSearch.get(i);
	                	// Real filtering:
	                	if(item.getName().toLowerCase().contains(constraint.toString().toLowerCase())){
	                		tempGlossaryList.add(item);
	                	}
	                    i++;
	                }

	                filterResults.values = tempGlossaryList;
	                filterResults.count = tempGlossaryList.size();
	                Log.i("Filtering", "Filter result count size"+filterResults.count);
	          }
	          return filterResults;
		}
	};
}
