package de.gabbo.forro_lyrics.listindexer.list;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class to store a single list entry
 */
public class ListEntry implements Serializable, Comparable<ListEntry> {

    private static final String TAG = ListEntry.class.getSimpleName();
    //public static final String PLACEHOLDER = "PLACEHOLDER";

    private String name;
	private String sortKey;
	private String albumName;
	private String artistName;
	private String trackName;
	private Map<Integer, String> lyrics;

    public ListEntry() {
        this.lyrics = new TreeMap<>();
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

    @Override
    public String toString() {
        return sortKey + "| " + name;
    }

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public void addLyric(Integer language, String lyric) {
        this.lyrics.put(language, lyric);
    }

    public Map<Integer, String> getLyrics() {
        return lyrics;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getTrackName() {
        return trackName;
    }

    /*public boolean isPlaceholder() {
        return artistName.equals(PLACEHOLDER);
    }*/

    @Override
    public int compareTo(@NonNull ListEntry another) {
        String me = artistName + albumName + trackName;
        String other = another.getArtistName() + another.getAlbumName() + another.getTrackName();
        return me.compareTo(other);
    }

    @Override
    public boolean equals(Object o) {
        //Log.d(TAG, "equals running on " + o);
        if (o instanceof ListEntry) {
            return this.compareTo((ListEntry) o) == 0;
        } else
            return super.equals(o);
    }

}
