package de.gabbo.forro_lyrics.listindexer;

import android.database.Cursor;

/**
 * Created by Gabor on 24.07.2016.
 * Interface for the search list functions, provides the excerpt of the database
 * to be displayed on the screen, and some support functions
 */
public interface ListDataInterface {

    /**
     * @return  index of the relevant data column in database
     */
    int getContentIndex();

    /**
     * @return  cursor the the relevant database entries
     */
    Cursor getDataCursor();

}
