package de.gabbo.forro_lyrics.listindexer;

import android.app.Activity;
import android.content.Intent;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import de.gabbo.forro_lyrics.CompatibilityWrapper;
import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.listindexer.list.ListEntry;
import de.gabbo.forro_lyrics.lyric.LyricActivity;
import de.gabbo.forro_lyrics.sql.providers.ArtistDataProvider;
import de.gabbo.forro_lyrics.sql.providers.ArtistTrackDataProvider;

/**
 * This class handles the listeners for the list view and entries
 * Created by gabor on 26.08.16.
 */
public class ForroListListener implements AbsListView.OnScrollListener, View.OnTouchListener, AdapterView.OnItemClickListener {

    private final String TAG = ForroListListener.class.getSimpleName();
    private final SpecialAlphabetIndexer mIndexer;
    private final FrameLayout mTitleLayout;
    private final int alphabetStartIndex;
    private final int alphabetEndIndex;
    private final int m_colorIndexerSelection;
    private final int m_drawableLetterlist;
    private TextView mTitleText;
    private RelativeLayout mSectionToastLayout;
    private TextView mSectionToastText;
    private LinearLayout mIndexerLayout;

    private ListDataInterface listDataInterface;
    private List<ListEntry> listEntries;
    private ListView mListView;
    private Activity activity;

    private int lastFirstVisibleItem = -1;
    private int lastSelectedPosition = -1;

    public ForroListListener(SpecialAlphabetIndexer indexer, ListDataInterface listDataInterface, List<ListEntry> listEntries,
                             LinearLayout mIndexerLayout, ListView listView, Activity activity) {
        mIndexer = indexer;
        this.listDataInterface = listDataInterface;
        this.listEntries = listEntries;
        this.mIndexerLayout = mIndexerLayout;
        mListView = listView;
        this.activity = activity;
        mTitleLayout = activity.findViewById(R.id.title_layout);
        mTitleText = activity.findViewById(R.id.title_text);
        mSectionToastLayout = activity.findViewById(R.id.section_toast_layout);
        TypedValue typedValue = new TypedValue();
        activity.getTheme().resolveAttribute(R.attr.colorIndexerSelection, typedValue, true);
        m_colorIndexerSelection = typedValue.data;
        activity.getTheme().resolveAttribute(R.attr.drawableIndexLetterlist, typedValue, true);
        m_drawableLetterlist = typedValue.resourceId;
        mSectionToastText = activity.findViewById(R.id.section_toast_text);
        alphabetStartIndex = 1;
        alphabetEndIndex = ListIndexerActivity.ALPHABET.length();
        initView();
    }

    private void initView() {
        mIndexerLayout.removeAllViews();
        for (int i = 0; i < ListIndexerActivity.ALPHABET.length(); i++) {
            TextView letterTextView = new TextView(activity);
            letterTextView.setText(String.valueOf(ListIndexerActivity.ALPHABET.charAt(i)));
            letterTextView.setTextSize(14f);
            letterTextView.setGravity(Gravity.CENTER);
            ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(32, 0, 1.0f);
            letterTextView.setLayoutParams(params);
            //letterTextView.setPadding(4, 0, 2, 0);
            mIndexerLayout.addView(letterTextView);
            mIndexerLayout.setBackgroundResource(m_drawableLetterlist);
        }
    }
    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) { }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount) {
        // firstVisibleItem corresponding to the index of AlphabetIndexer(eg, B-->Alphabet index is 2)
        int sectionIndex = mIndexer.getSectionForPosition(firstVisibleItem);
        //next section Index corresponding to the positon in the listview
        int nextSectionPosition = mIndexer.getPositionForSection(sectionIndex + 1);
        //Log.d(TAG, "onScroll()-->firstVisibleItem="+firstVisibleItem+", sectionIndex=" +sectionIndex+", nextSectionPosition="+nextSectionPosition);
        if(firstVisibleItem != lastFirstVisibleItem) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) mTitleLayout.getLayoutParams();
            params.topMargin = 0;
            mTitleLayout.setLayoutParams(params);
            if (listDataInterface instanceof ArtistTrackDataProvider) {
                //Log.d(TAG, "setTitleText to: " + listEntries.get(firstVisibleItem).getAlbumName() + "|" + sectionIndex+ "|" + firstVisibleItem);
                mTitleText.setText(listEntries.get(firstVisibleItem).getAlbumName());
            } else {
                //Log.d(TAG, "setTitleText to: " + alphabet.charAt(sectionIndex) + "|" + sectionIndex);
                mTitleText.setText(String.valueOf(mIndexer.getSections()[sectionIndex]));
            }
            mIndexerLayout.getChildAt(mIndexer.getSkippedIndex(sectionIndex)).setBackgroundColor(m_colorIndexerSelection);
            lastFirstVisibleItem = firstVisibleItem;
        }
        // update AlphabetIndexer background
        if(mIndexer.getSkippedIndex(sectionIndex) != lastSelectedPosition) {
            if(lastSelectedPosition != -1) {
                for (int index = lastSelectedPosition; index != mIndexer.getSkippedIndex(sectionIndex);) {
                    mIndexerLayout.getChildAt(index).setBackgroundColor(CompatibilityWrapper.getColor(activity.getApplicationContext(), android.R.color.transparent));
                    if (index < mIndexer.getSkippedIndex(sectionIndex)) {
                        index++;    // clear the alphabet downward
                    } else {
                        index--;    // clear the alphabet upward
                    }
                }
            }
            lastSelectedPosition = mIndexer.getSkippedIndex(sectionIndex);
        }
        if(nextSectionPosition == firstVisibleItem + 1) {
            View childView = view.getChildAt(0);
            if(childView != null) {
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) mTitleLayout.getLayoutParams();
                if(params.topMargin != 0) {
                    params.topMargin = 0;
                    mTitleLayout.setLayoutParams(params);
                }
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float alphabetHeight = mIndexerLayout.getHeight();
        float y = event.getY();
        int sectionPosition = (int) ((y / alphabetHeight) / (((float)alphabetStartIndex) / alphabetEndIndex));
        //Log.d(TAG, "Touch selected pos " + sectionPosition + "/" + ListIndexerActivity.ALPHABET.charAt(sectionPosition));
        if (sectionPosition < alphabetStartIndex - 1) {
            sectionPosition = alphabetStartIndex - 1;
        } else if (sectionPosition > alphabetEndIndex - 1) {
            sectionPosition = alphabetEndIndex - 1;
        }
        if(lastSelectedPosition != sectionPosition) {
            if(-1 != lastSelectedPosition){
                mIndexerLayout.getChildAt(lastSelectedPosition).setBackgroundColor(CompatibilityWrapper.getColor(activity.getApplicationContext(), android.R.color.transparent));
            }
            lastSelectedPosition = sectionPosition;
        }
        String sectionLetter = String.valueOf(ListIndexerActivity.ALPHABET.charAt(sectionPosition));
        int position = mIndexer.getPositionForSectionSearch(sectionPosition);
        TextView textView = (TextView) mIndexerLayout.getChildAt(sectionPosition);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mIndexerLayout.setBackgroundResource(m_drawableLetterlist);
                textView.setBackgroundColor(m_colorIndexerSelection);
                mSectionToastLayout.setVisibility(View.VISIBLE);
                mSectionToastText.setText(sectionLetter);
                //mListView.smoothScrollToPositionFromTop(position,0,1);
                CompatibilityWrapper.smoothScrollToPositionFromTop(mListView, position, 0, 1);
                break;
            case MotionEvent.ACTION_MOVE:
                mIndexerLayout.setBackgroundResource(m_drawableLetterlist);
                textView.setBackgroundColor(m_colorIndexerSelection);
                mSectionToastLayout.setVisibility(View.VISIBLE);
                mSectionToastText.setText(sectionLetter);
                //mListView.smoothScrollToPositionFromTop(position,0,1);
                CompatibilityWrapper.smoothScrollToPositionFromTop(mListView, position, 0, 1);
                break;
            case MotionEvent.ACTION_UP:
                //mIndexerLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                mSectionToastLayout.setVisibility(View.GONE);
                v.performClick();
            default:
                mSectionToastLayout.setVisibility(View.GONE);
                break;
        }
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ListEntry item = (ListEntry) parent.getItemAtPosition(position);
        if (listDataInterface instanceof ArtistDataProvider) {
            //Log.d(TAG, "Selected artist: : " + item.getName());
            Intent i = new Intent(activity.getApplicationContext(), ListIndexerActivity.class);
            i.putExtra(ListIndexerActivity.LIST_TYPE_NAME, ArtistTrackDataProvider.NAME_ID);
            i.putExtra(ArtistTrackDataProvider.ARTIST_ID, item.getName());
            activity.startActivity(i);
        } else {
            //Log.d(TAG, "ListEntry: " + item.getName());
            Intent i = new Intent(activity.getApplicationContext(), LyricActivity.class);
            i.putExtra(LyricActivity.ENTRY_NAME, item);
            activity.startActivity(i);
        }
    }

}
