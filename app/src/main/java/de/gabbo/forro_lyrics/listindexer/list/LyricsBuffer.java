package de.gabbo.forro_lyrics.listindexer.list;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.List;

/**
 * Created by gabor on 01.10.16.
 * This class stores the buffered lyrics data and the ListEntry container
 */
public class LyricsBuffer extends ForroCursorLoader {

    private final Cursor data;
    private final List<ListEntry> listEntries;

    public LyricsBuffer(Context context, Cursor data, List<ListEntry> listEntries) {
        super(context);
        this.data = data;
        this.listEntries = listEntries;
    }

    @Override
    public List<ListEntry> getListEntries() {
        return listEntries;
    }

    @Override
    public Cursor loadInBackground() {
        Log.d("Loader", "Getting data from buffer");
        return data;
    }
}
