package de.gabbo.forro_lyrics.activities;

import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import de.gabbo.forro_lyrics.InitTask;

/**
 * Created by Gabor on 22.07.2016.
 * Splash screen activity
 */
public class SplashActivity extends AppCompatActivity {

    /** Duration of wait **/
    //private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InitTask initTasks = new InitTask(this);
        new Handler().post(initTasks);
        //new Handler().postDelayed(initTasks, SPLASH_DISPLAY_LENGTH);
    }
}
