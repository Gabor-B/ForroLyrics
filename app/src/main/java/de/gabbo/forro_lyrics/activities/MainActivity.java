package de.gabbo.forro_lyrics.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.listindexer.ListIndexerActivity;
import de.gabbo.forro_lyrics.localization.ChangelogDialog;
import de.gabbo.forro_lyrics.localization.LocaleDialog;
import de.gabbo.forro_lyrics.localization.SettingsHelper;
import de.gabbo.forro_lyrics.localization.ThemeDialog;
import de.gabbo.forro_lyrics.services.PlayedTrackReceiverConnection;
import de.gabbo.forro_lyrics.services.PlayedTrackReceiverService;
import de.gabbo.forro_lyrics.sql.providers.ArtistDataProvider;
import de.gabbo.forro_lyrics.sql.providers.TrackDataProvider;

/**
 * Created by Gabor on 22.07.2016.
 * Main activity for the app
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    boolean mIsBound;
    private PlayedTrackReceiverConnection mConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsHelper.setDarkThemeInApp(this);
        SettingsHelper.setLocale(getBaseContext(), SettingsHelper.getLanguage(getBaseContext()));
        setContentView(R.layout.activity_main);
        mConnection = new PlayedTrackReceiverConnection(null);
        bindService(new Intent(this, PlayedTrackReceiverService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Main OnDestroy");
        doUnbindService();
    }

    void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mConnection.getMessengerService() != null) {
                try {
                    Message msg = Message.obtain(null, PlayedTrackReceiverService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.getHandler();
                    mConnection.getMessengerService().send(msg);
                }
                catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
            //textStatus.setText("Unbinding.");
        }
    }

    public void startArtistListActivity(View view) {
        Intent i = new Intent(getApplicationContext(), ListIndexerActivity.class);
        i.putExtra(ListIndexerActivity.LIST_TYPE_NAME, ArtistDataProvider.NAME_ID);
        startActivity(i);
    }

    public void startTitleListActivity(View view) {
        Intent i = new Intent(getApplicationContext(), ListIndexerActivity.class);
        i.putExtra(ListIndexerActivity.LIST_TYPE_NAME, TrackDataProvider.NAME_ID);
        startActivity(i);
    }

    public void startContributeActivity(View view) {
        Intent i = new Intent(getApplicationContext(), ContributeActivity.class);
        startActivity(i);
    }

    public void startSearchTextActivity(View view) {
        Intent i = new Intent(getApplicationContext(), SearchTextContentActivity.class);
        startActivity(i);
    }

    public void startGetPlayedTextActivity(View view) {
        Intent i = new Intent(getApplicationContext(), GetPlayedTrackActivity.class);
        startActivity(i);
    }

    public void startWebFormContribution(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.contribution_webpage)));
        startActivity(browserIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_set_language) {
            LocaleDialog dialog = new LocaleDialog(MainActivity.this);
            dialog.createDialog();
            return true;
        } else if (id == R.id.action_reset_database) {
            Intent i = new Intent(getApplicationContext(), ResetDBActivity.class);
            startActivity(i);
            return true;
        } else if (id == R.id.action_toggle_dark_light) {
            ThemeDialog dialog = new ThemeDialog(MainActivity.this);
            dialog.createDialog();
            return true;
        } else if (id == R.id.action_help) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.help_webpage)));
            startActivity(browserIntent);
            return true;
        } else if (id == R.id.action_changelog) {
            ChangelogDialog dialog = new ChangelogDialog(MainActivity.this);
            dialog.createDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
