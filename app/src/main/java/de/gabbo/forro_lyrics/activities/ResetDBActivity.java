package de.gabbo.forro_lyrics.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

import de.gabbo.forro_lyrics.InitTask;
import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;
import de.gabbo.forro_lyrics.sql.SQLiteDefines;

/**
 * Created by Gabor on 06.08.2016.
 * Activity to reset the local database
 */
public class ResetDBActivity extends AppCompatActivity {

    private static final String TAG = ResetDBActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Put up the Yes/No message box
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(getString(R.string.action_reset_database))
                .setMessage(getString(R.string.confirmation_dialog))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(getString(R.string.confirmation_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Yes button clicked, do something
                        Toast.makeText(ResetDBActivity.this, getString(R.string.reset_database_feedback),
                                Toast.LENGTH_SHORT).show();
                        resetDatabase();
                        setResult(AppCompatActivity.RESULT_OK);
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.confirmation_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Yes button clicked, do something
                        setResult(AppCompatActivity.RESULT_OK);
                        finish();
                    }
                })
                .show();
    }

    /**
     * Close the database and overwrite with the default one. Then reopen the DB
     */
    public void resetDatabase() {
        String storagePath = getFilesDir().getParentFile() + "/databases";
        File outFile = new File(storagePath, SQLiteDefines.NAME_DB);
        Log.d(TAG, "Outdir exists: " + outFile.getParentFile().exists() + "| created: " + outFile.getParentFile().mkdirs());
        SQLiteDataSource sql = SQLiteDataSource.getInstance(getApplicationContext());
        sql.close();
        if (outFile.exists()) {
            boolean result = outFile.delete();
            Log.d(TAG, "old DB deleted: " + result + ", file exists: " + outFile.exists());
        }
        InitTask.copyDatabaseFile(getAssets(), outFile);
        sql.open();
    }
}