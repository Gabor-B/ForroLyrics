package de.gabbo.forro_lyrics.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.listindexer.list.ListEntry;
import de.gabbo.forro_lyrics.listindexer.ListIndexerActivity;
import de.gabbo.forro_lyrics.localization.SettingsHelper;
import de.gabbo.forro_lyrics.lyric.LyricActivity;
import de.gabbo.forro_lyrics.services.PlayedTrackReceiverConnection;
import de.gabbo.forro_lyrics.services.PlayedTrackReceiverInterface;
import de.gabbo.forro_lyrics.services.PlayedTrackReceiverService;
import de.gabbo.forro_lyrics.sql.providers.ArtistTrackDataProvider;
import de.gabbo.forro_lyrics.sql.SQLiteDBReader;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;
import de.gabbo.forro_lyrics.sql.SQLiteDefines;
import de.gabbo.forro_lyrics.sql.providers.SimilarTrackDataProvider;

/**
 * Created by gabor on 06.08.16.
 * Activity to get the played music data and to start a search for the entries
 */
public class GetPlayedTrackActivity extends AppCompatActivity {

    private static final String TAG = GetPlayedTrackActivity.class.getSimpleName();

    private EditText editArtist;
    private EditText editAlbum;
    private EditText editTrack;
    private TextView textStatus;
    private LinearLayout layoutTrackData;
    private LinearLayout layoutHint;
    final Messenger mMessengerHandler = new Messenger(new IncomingHandler());
    private final PlayedTrackReceiverConnection mConnection = new PlayedTrackReceiverConnection(mMessengerHandler);
    private boolean mIsBound;
    private Button button1;
    private Button button2;
    private Button button3;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle data = msg.getData();
            if (msg.what == PlayedTrackReceiverInterface.ValueEnum.ARTIST.getId()) {
                ArrayList<String> values = data.getStringArrayList(PlayedTrackReceiverService.MSG_VALUE_ID);
                if (values != null && values.size() >= 3) {
                    editArtist.setText(values.get(0));
                    editAlbum.setText(values.get(1));
                    editTrack.setText(values.get(2));
                }
            } else if (msg.what == PlayedTrackReceiverInterface.ValueEnum.PLAYING.getId()) {
                Boolean playing = data.getBoolean(PlayedTrackReceiverService.MSG_VALUE_ID);
                setLayoutVisibility(playing);
                if (!playing) {
                    textStatus.setText(getString(R.string.text_no_music_played));
                } else {
                    textStatus.setText("");
                }
                super.handleMessage(msg);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsHelper.setDarkThemeInApp(this);
        setContentView(R.layout.activity_played_track);
        bindService(new Intent(this, PlayedTrackReceiverService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
        this.editArtist = (EditText) this.findViewById(R.id.edit_playedtrack_artist);
        this.editAlbum = (EditText) this.findViewById(R.id.edit_playedtrack_album);
        this.editTrack = (EditText) this.findViewById(R.id.edit_playedtrack_track);
        this.textStatus = (TextView) this.findViewById(R.id.text_playedtrack_status);
        this.layoutHint = (LinearLayout) this.findViewById(R.id.layout_playedtrack_hint);
        this.layoutTrackData = (LinearLayout) this.findViewById(R.id.layout_playedtrack_data);
        this.button1 = (Button) this.findViewById(R.id.button_current_lookup_track);
        this.button2 = (Button) this.findViewById(R.id.button_current_lookup_artist);
        this.button3 = (Button) this.findViewById(R.id.button_current_lookup);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    private void doUnbindService() {
        if (mIsBound) {
            if (mConnection.getMessengerService() != null) {
                try {
                    Message msg = Message.obtain(null, PlayedTrackReceiverService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mConnection.getHandler();
                    mConnection.getMessengerService().send(msg);
                }
                catch (RemoteException e) {
                    Log.e(TAG, e.getMessage());
                }
            }
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    private void setLayoutVisibility(Boolean isMusicActive) {
        if (isMusicActive) {
            layoutHint.setVisibility(View.GONE);
            layoutTrackData.setVisibility(View.VISIBLE);
            button1.setVisibility(View.VISIBLE);
            button2.setVisibility(View.VISIBLE);
            button3.setVisibility(View.VISIBLE);
        } else {
            layoutHint.setVisibility(View.VISIBLE);
            layoutTrackData.setVisibility(View.GONE);
            button1.setVisibility(View.GONE);
            button2.setVisibility(View.GONE);
            button3.setVisibility(View.GONE);
        }
    }

    public void startLookupArtist(View view) {
        String artistName = editArtist.getText().toString().trim();
        Log.d(TAG, "Look for artist: " + artistName);
        if (artistName.length() > 0) {
            Intent i = new Intent(getApplicationContext(), ListIndexerActivity.class);
            i.putExtra(ListIndexerActivity.LIST_TYPE_NAME, ArtistTrackDataProvider.NAME_ID);
            i.putExtra(ArtistTrackDataProvider.ARTIST_ID, artistName);
            startActivity(i);
        } else {
            Toast.makeText(GetPlayedTrackActivity.this, R.string.text_currenttrack_no_artist, Toast.LENGTH_SHORT).show();
        }
    }

    public void startLookupTrack(View view) {
        String trackName = editTrack.getText().toString().trim();
        if (trackName.length() > 0) {
            Log.d(TAG, "Look for track: " + trackName);
            Intent i = new Intent(getApplicationContext(), ListIndexerActivity.class);
            i.putExtra(ListIndexerActivity.LIST_TYPE_NAME, SimilarTrackDataProvider.NAME_ID);
            i.putExtra(SimilarTrackDataProvider.TRACK_ID, trackName);
            startActivity(i);
        } else {
            Toast.makeText(GetPlayedTrackActivity.this, R.string.text_currenttrack_no_track, Toast.LENGTH_SHORT).show();
        }
    }

    public void startLookupArtistTrack(View view) {
        String artistName = editArtist.getText().toString().trim();
        String trackName = editTrack.getText().toString().trim();
        if (trackName.length() > 0 && artistName.length() > 0) {

            Log.d(TAG, "Look for specific track: " + editArtist.getText().toString().trim() + ":" + editTrack.getText().toString().trim());
            SQLiteDataSource database = SQLiteDataSource.getInstance(getApplicationContext());
            ListEntry entry = new ListEntry();
            entry.setArtistName(editArtist.getText().toString().trim());
            entry.setAlbumName(editAlbum.getText().toString().trim());
            entry.setTrackName(editTrack.getText().toString().trim());
            Cursor cursor = database.getSpecificTracks(entry.getArtistName(), entry.getTrackName());
            if (cursor.moveToFirst()) {
                do {
                    Integer language = cursor.getInt(SQLiteDBReader.getIndex(SQLiteDefines.COLUMN_LANGUAGE));
                    String lyric = cursor.getString(SQLiteDBReader.getIndex(SQLiteDefines.COLUMN_LYRIC));
                    entry.addLyric(language, lyric);
                } while (cursor.moveToNext());
            }
            Intent i = new Intent(getApplicationContext(), LyricActivity.class);
            i.putExtra(LyricActivity.ENTRY_NAME, entry);
            startActivity(i);
            if (entry.getLyrics().size() == 0) {
                Toast.makeText(GetPlayedTrackActivity.this, R.string.text_currenttrack_no_entry_found, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(GetPlayedTrackActivity.this, R.string.text_currenttrack_no_artisttrack, Toast.LENGTH_SHORT).show();
        }
    }

}
