package de.gabbo.forro_lyrics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.TreeMap;

import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.Utilities;
import de.gabbo.forro_lyrics.localization.SettingsHelper;
import de.gabbo.forro_lyrics.representation.LangEnum;
import de.gabbo.forro_lyrics.sql.SQLiteDataSource;

/**
 * Created by Gabor on 06.08.2016.
 * Activity for the contribute lyrics function
 */
public class ContributeActivity extends AppCompatActivity {

    private static final String TAG = ContributeActivity.class.getSimpleName();
    private Spinner spinner;
    private TreeMap<Integer, LangEnum> spinnerMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsHelper.setDarkThemeInApp(this);
        setContentView(R.layout.activity_contribute);
        ArrayList<String> spinnerArray = new ArrayList<>();
        Bundle extras = getIntent().getExtras();
        ArrayList<Integer> langId = null;
        if (extras != null) {
            String artistName = extras.getString(getString(R.string.artist));
            String albumName = extras.getString(getString(R.string.album));
            String trackName = extras.getString(getString(R.string.track));
            String lyrics = extras.getString(getString(R.string.lyric));
            if (lyrics != null) {
                sendContribution(artistName, albumName, trackName, extras.getInt(getString(R.string.language)), lyrics, false);
                return;
            } else {
                langId = extras.getIntegerArrayList(getString(R.string.language));
            }
            EditText txtArtist = (EditText) findViewById(R.id.edit_contribute_artist);
            txtArtist.setText(artistName);
            EditText txtAlbum = (EditText) findViewById(R.id.edit_contribute_album);
            txtAlbum.setText(albumName);
            EditText txtTrack = (EditText) findViewById(R.id.edit_contribute_track);
            txtTrack.setText(trackName);
        }
        spinnerMap = new TreeMap<>();
        for (int index = 0; index < LangEnum.values().length; index++) {
            if (langId == null || !langId.contains(index)) {
                int id = Utilities.getStringIdentifier(getApplicationContext(), LangEnum.values()[index].getResourceId());
                spinnerMap.put(spinnerArray.size(), LangEnum.values()[index]);
                spinnerArray.add(getString(id));
                //Log.d(TAG, "adding string to spinner " + id + "/" + getString(id));
            }
        }
        spinner = (Spinner) findViewById(R.id.spinner_contribute_lang);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
    }

    /**
     * Check entered values and prepare email
     * @param view  source view
     */
    public void contributeSendButtonAction(View view) {
        EditText txtArtist = (EditText) findViewById(R.id.edit_contribute_artist);
        String artist = txtArtist.getText().toString();
        EditText txtAlbum = (EditText) findViewById(R.id.edit_contribute_album);
        String album = txtAlbum.getText().toString();
        EditText txtTrack = (EditText) findViewById(R.id.edit_contribute_track);
        String track = txtTrack.getText().toString();
        EditText txtLyrics = (EditText) findViewById(R.id.edit_contribute_lyric);
        String lyrics = txtLyrics.getText().toString();
        Integer language = spinnerMap.get(spinner.getSelectedItemPosition()).getId();
        if (artist.length() == 0) {
            Toast.makeText(ContributeActivity.this, getString(R.string.error_contribute_no_artist), Toast.LENGTH_SHORT).show();
        }
        if (album.length() == 0) {
            Toast.makeText(ContributeActivity.this, getString(R.string.error_contribute_no_album), Toast.LENGTH_SHORT).show();
        }
        if (track.length() == 0) {
            Toast.makeText(ContributeActivity.this, getString(R.string.error_contribute_no_track), Toast.LENGTH_SHORT).show();
        }
        if (lyrics.length() == 0) {
            Toast.makeText(ContributeActivity.this, getString(R.string.error_contribute_no_lyrics), Toast.LENGTH_SHORT).show();
        }
        if (artist.length() == 0 || album.length() == 0 || track.length() == 0 || lyrics.length() == 0) {
            return;
        }
        sendContribution(artist, album, track, language, lyrics, true);
    }

    private void sendContribution(String artist, String album, String track, Integer language, String lyrics, boolean addToLocalDatabase) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{getString(R.string.email_address)});
        i.putExtra(Intent.EXTRA_SUBJECT, "Contribute new lyrics");
        i.putExtra(Intent.EXTRA_TEXT   , "App lyrics data contribution\n\n"+
                getString(R.string.feedback_begin) + "\n" +
                "artist=" + artist + "\nalbum=" + album + "\ntrack=" + track + "\nlanguage=" + language +
                "\nlyrics=" + lyrics + "\n" + getString(R.string.feedback_terminate));
        try {
            startActivity(Intent.createChooser(i, getString(R.string.action_send_mail)));
            if (addToLocalDatabase) {
                Toast.makeText(ContributeActivity.this, getString(R.string.text_contribute_added), Toast.LENGTH_SHORT).show();
                SQLiteDataSource.getInstance(getApplicationContext()).addTrack(artist, album, track, language, lyrics);
            }
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ContributeActivity.this, getString(R.string.error_no_email), Toast.LENGTH_SHORT).show();
        }
        setResult(AppCompatActivity.RESULT_OK);
        finish();

    }

}
