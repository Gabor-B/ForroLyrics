package de.gabbo.forro_lyrics.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import de.gabbo.forro_lyrics.CompatibilityWrapper;
import de.gabbo.forro_lyrics.R;
import de.gabbo.forro_lyrics.listindexer.ListIndexerActivity;
import de.gabbo.forro_lyrics.localization.SettingsHelper;
import de.gabbo.forro_lyrics.sql.providers.SearchedTrackDataProvider;

/**
 * Created by Gabor on 09.08.2016.
 * Activity for the search lyric text function
 */
public class SearchTextContentActivity extends AppCompatActivity {

    private static final String TAG = SearchTextContentActivity.class.getSimpleName();
    ArrayList<EditText> editList = new ArrayList<>();
    ArrayList<ImageButton> buttonList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsHelper.setDarkThemeInApp(this);
        setContentView(R.layout.activity_text_contains);
        editList.add((EditText) findViewById(R.id.edit_searchtext_1));
        ImageButton deleteButton = (ImageButton) findViewById(R.id.button_searchtext_1);
        buttonList.add(deleteButton);
        deleteButton.setId(0);
        findViewById(R.id.button_textsearch_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEntryToSearch();
            }
        });
    }

    /**
     * Add a new set of editbox and delete button to the screen
     */
    private void addEntryToSearch() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setOrientation(LinearLayout.HORIZONTAL);
        Resources r = getResources();
        int px = (int) r.getDimension(R.dimen.padding_searchtext);
        layout.setPadding(px, 0, px, 0);
        EditText editText = new EditText(this);
        CompatibilityWrapper.setTextAppearance(editText, this, android.R.style.TextAppearance_Medium);
        editText.setSingleLine(true);
        LinearLayout.LayoutParams editLayout = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        editLayout.gravity = Gravity.CENTER_VERTICAL;
        editLayout.weight = 0.7f;
        layout.addView(editText, editLayout);
        ImageButton deleteButton = new ImageButton(getApplicationContext());
        LinearLayout.LayoutParams buttonLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonLayout.gravity = Gravity.RIGHT;
        deleteButton.setBackgroundResource(R.drawable.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeButton(v);
            }
        });
        layout.addView(deleteButton, buttonLayout);
        LinearLayout view = (LinearLayout) findViewById(R.id.search_buttons_layout);
        view.addView(layout, params);
        deleteButton.setId(buttonList.size());
        buttonList.add(deleteButton);
        editList.add(editText);
    }

    /**
     * Remove the clicked delete button and the assigned edittext from the screen
     * @param view  source view
     */
    public void removeButton(View view) {
        int viewIndex = view.getId();
        LinearLayout layout = (LinearLayout) buttonList.get(viewIndex).getParent();
        editList.remove(viewIndex);
        buttonList.remove(viewIndex);
        layout.removeAllViewsInLayout();
        ((LinearLayout) layout.getParent()).removeView(layout);
        for (int index = 0; index < editList.size(); index++) {
            editList.get(index).setId(index);
            buttonList.get(index).setId(index);
        }
    }

    /**
     * Read user entries and start the search for track text activity
     * @param view  source view
     */
    public void searchLyricsForTexts(View view) {
        ArrayList<String> textEntries = new ArrayList<>();
        for (EditText editText : editList) {
            if (editText.getText().toString().length() > 0) {
                textEntries.add(editText.getText().toString().trim());
            }
        }
        if (textEntries.size() == 0) {
            Toast.makeText(SearchTextContentActivity.this, R.string.error_no_searchtext, Toast.LENGTH_SHORT).show();
        } else {
            //Log.d(TAG, "searching for text entries: " + textEntries);
            Intent i = new Intent(getApplicationContext(), ListIndexerActivity.class);
            i.putExtra(ListIndexerActivity.LIST_TYPE_NAME, SearchedTrackDataProvider.NAME_ID);
            i.putStringArrayListExtra(SearchedTrackDataProvider.NAME_ID, textEntries);
            startActivity(i);
        }
    }
}
