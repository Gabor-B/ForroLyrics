ForróLyrics was created to help people like me to understand the portuguese
lyrics in forró songs. And to sing along :).

Functions:

* Search for lyrics by artist or track name
* Search for text passages inside the lyrics
* Lyric text in multiple languages - where available
* Get currently played music track name, if played in the background
* Contribute new lyrics or translations

The app contains some lyrics assembled by the author of this app, the database
will grow with time. You can contribute too, also with translations if you like!
